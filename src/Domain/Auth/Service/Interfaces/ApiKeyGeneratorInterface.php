<?php

declare(strict_types=1);

namespace Parser\Domain\Auth\Service\Interfaces;

use Parser\Domain\Auth\ValueObject\ApiKey;

/**
 * Interface for api key generator.
 */
interface ApiKeyGeneratorInterface
{
    /**
     * Generates new random api key.
     *
     * @return ApiKey
     */
    public function generate() : ApiKey;
}
