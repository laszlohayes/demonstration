<?php

declare(strict_types=1);

namespace Parser\Domain\Auth\Service\Interfaces;

use Parser\Domain\Auth\ValueObject\EncryptedPassword;
use Parser\Domain\Auth\ValueObject\PlainPassword;

/**
 * Interface for password encoders.
 */
interface PasswordEncoderInterface
{
    /**
     * Returns hashed password.
     *
     * @param PlainPassword $password
     *
     * @return EncryptedPassword
     */
    public function encode(PlainPassword $password) : EncryptedPassword;

    /**
     * Returns true if password is correct.
     *
     * @param EncryptedPassword $encryptedPassword
     * @param PlainPassword     $password
     *
     * @return bool
     */
    public function verify(EncryptedPassword $encryptedPassword, PlainPassword $password) : bool;
}
