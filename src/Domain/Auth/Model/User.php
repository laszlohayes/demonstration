<?php

declare(strict_types=1);

namespace Parser\Domain\Auth\Model;

use Parser\Domain\Auth\ValueObject\ApiKey;
use Parser\Domain\Auth\ValueObject\EncryptedPassword;
use Parser\Domain\SharedKernel\ValueObject\UserId;

/**
 * Aggregation root for user auth.
 */
class User
{
    /**
     * @var UserId
     */
    private $id;

    /**
     * @var ApiKey
     */
    private $apiKey;

    /**
     * @var EncryptedPassword
     */
    private $password;

    /**
     * @param UserId $userId
     */
    public function __construct(UserId $userId)
    {
        $this->id = $userId;
    }

    /**
     * @return UserId
     */
    public function getId() : UserId
    {
        return $this->id;
    }

    /**
     * @return null|ApiKey
     */
    public function getApiKey() : ?ApiKey
    {
        return $this->apiKey;
    }

    /**
     * @param ApiKey $apiKey
     */
    public function setApiKey(ApiKey $apiKey) : void
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return null|EncryptedPassword $password
     */
    public function getPassword() : ?EncryptedPassword
    {
        return $this->password;
    }

    /**
     * @param EncryptedPassword $password
     */
    public function setPassword(EncryptedPassword $password) : void
    {
        $this->password = $password;
    }
}
