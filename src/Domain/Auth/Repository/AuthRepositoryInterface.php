<?php

declare(strict_types=1);

namespace Parser\Domain\Auth\Repository;

use Parser\Domain\Auth\Exception\UserNotFoundException;
use Parser\Domain\Auth\Model\User;
use Parser\Domain\Auth\ValueObject\ApiKey;
use Parser\Domain\SharedKernel\ValueObject\UserId;

/**
 * Interface for auth repository.
 */
interface AuthRepositoryInterface
{
    /**
     * Returns user auth model.
     *
     * @param UserId $id
     *
     * @return User
     */
    public function ofId(UserId $id) : User;

    /**
     * Saves model.
     *
     * @param User $user
     */
    public function save(User $user) : void;

    /**
     * Returns user auth model by api key.
     *
     * @param ApiKey $apiKey
     *
     * @return User
     * @throws UserNotFoundException
     */
    public function byApiKey(ApiKey $apiKey) : User;
}
