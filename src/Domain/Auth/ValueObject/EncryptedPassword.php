<?php

declare(strict_types=1);

namespace Parser\Domain\Auth\ValueObject;

use Parser\Domain\Auth\Exception\InvalidEncryptedPasswordException;

/**
 * Value object for user encrypted password.
 */
class EncryptedPassword
{
    /**
     * @var string
     */
    private $password;

    /**
     * @param string $password
     *
     * @throws InvalidEncryptedPasswordException
     */
    public function __construct(string $password)
    {
        if (strlen(trim($password)) === 0) {
            throw new InvalidEncryptedPasswordException($password);
        }

        $this->password = $password;
    }

    /**
     * Returns whether two values are equal.
     *
     * @param EncryptedPassword $other
     *
     * @return bool
     */
    public function equals(EncryptedPassword $other) : bool
    {
        return $this->password === $other->getPassword();
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->password;
    }
}
