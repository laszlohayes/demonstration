<?php

declare(strict_types=1);

namespace Parser\Domain\Auth\ValueObject;

use Parser\Domain\Auth\Exception\InvalidPlainPasswordException;

/**
 * Value object for user plain password.
 */
class PlainPassword
{
    /**
     * @var string
     */
    private $password;

    /**
     * @param string $password
     *
     * @throws InvalidPlainPasswordException
     */
    public function __construct(string $password)
    {
        if (strlen(trim($password)) === 0) {
            throw new InvalidPlainPasswordException($password);
        }

        $this->password = $password;
    }

    /**
     * Returns whether two values are equal.
     *
     * @param PlainPassword $other
     *
     * @return bool
     */
    public function equals(PlainPassword $other) : bool
    {
        return $this->password === $other->getPassword();
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->password;
    }
}
