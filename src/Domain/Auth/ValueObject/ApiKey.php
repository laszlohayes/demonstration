<?php

declare(strict_types=1);

namespace Parser\Domain\Auth\ValueObject;

use Parser\Domain\Auth\Exception\InvalidApiKeyException;

/**
 * Value object for api key.
 */
class ApiKey
{
    /**
     * @var string
     */
    private $apiKey;

    /**
     * @param string $apiKey
     *
     * @throws InvalidApiKeyException
     */
    public function __construct(string $apiKey)
    {
        if (!strlen(trim($apiKey))) {
            throw new InvalidApiKeyException($apiKey);
        }

        $this->apiKey = $apiKey;
    }

    /**
     * Returns whether two values are equal.
     *
     * @param ApiKey $other
     *
     * @return bool
     */
    public function equals(ApiKey $other) : bool
    {
        return $this->apiKey === $other->getApiKey();
    }

    /**
     * @return string
     */
    public function getApiKey() : string
    {
        return $this->apiKey;
    }
}
