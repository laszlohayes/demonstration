<?php

declare(strict_types=1);

namespace Parser\Domain\Auth\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception for bad user credentials.
 */
class BadCredentialsException extends \Exception implements DomainExceptionInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        parent::__construct('Bad user credentials.');
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return 'ace87427-507f-4e4a-b3a0-2e42878c302a';
    }
}
