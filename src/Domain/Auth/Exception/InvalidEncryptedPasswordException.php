<?php

declare(strict_types=1);

namespace Parser\Domain\Auth\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception for invalid encrypted password.
 */
class InvalidEncryptedPasswordException extends \Exception implements DomainExceptionInterface
{
    /**
     * @var string
     */
    private $encryptedPassword;

    /**
     * @param string $encryptedPassword
     */
    public function __construct(string $encryptedPassword)
    {
        parent::__construct(sprintf('Invalid encrypted password %s.', $encryptedPassword));

        $this->encryptedPassword = $encryptedPassword;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return 'dc2c3198-cb6c-4b43-8837-8df684767654';
    }

    /**
     * @return string
     */
    public function getEncryptedPassword() : string
    {
        return $this->encryptedPassword;
    }
}
