<?php

declare(strict_types=1);

namespace Parser\Domain\Auth\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception for invalid plain password.
 */
class InvalidPlainPasswordException extends \Exception implements DomainExceptionInterface
{
    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @param string $plainPassword
     */
    public function __construct(string $plainPassword)
    {
        parent::__construct(sprintf('Invalid plain password %s.', $plainPassword));

        $this->plainPassword = $plainPassword;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return 'fa713a32-7b46-4286-a17a-82e3a3273004';
    }

    /**
     * @return string
     */
    public function getPlainPassword() : string
    {
        return $this->plainPassword;
    }
}
