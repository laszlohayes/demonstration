<?php

declare(strict_types=1);

namespace Parser\Domain\Auth\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception for invalid user credentials.
 */
class InvalidCredentialsException extends \Exception implements DomainExceptionInterface
{
    /**
     * InvalidCredentialsException constructor.
     */
    public function __construct()
    {
        parent::__construct(sprintf('Invalid user credentials.'));
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return 'b47193c9-f526-47a1-b185-f791908d0fc7';
    }
}
