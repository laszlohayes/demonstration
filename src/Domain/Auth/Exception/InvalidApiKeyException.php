<?php

declare(strict_types=1);

namespace Parser\Domain\Auth\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception for invalid api key.
 */
class InvalidApiKeyException extends \Exception implements DomainExceptionInterface
{
    /**
     * @var string
     */
    private $apiKey;

    /**
     * @param string $apiKey
     */
    public function __construct(string $apiKey)
    {
        parent::__construct(sprintf('Invalid api key %s.', $apiKey));

        $this->apiKey = $apiKey;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return '15dbb898-d663-4278-a6c1-a683072209a1';
    }

    /**
     * @return string
     */
    public function getApiKey() : string
    {
        return $this->apiKey;
    }
}
