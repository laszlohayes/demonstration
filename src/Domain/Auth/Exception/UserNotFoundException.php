<?php

declare(strict_types=1);

namespace Parser\Domain\Auth\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception for not found user.
 */
class UserNotFoundException extends \Exception implements DomainExceptionInterface
{
    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        parent::__construct(sprintf("User '%s' not found.", $value));
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return 'af46a620-1d79-4f9b-a5e8-4ffe9823071f';
    }
}
