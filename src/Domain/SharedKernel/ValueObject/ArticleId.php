<?php

declare(strict_types=1);

namespace Parser\Domain\SharedKernel\ValueObject;

use Parser\Domain\SharedKernel\Exception\InvalidArticleIdException;
use Ramsey\Uuid\UuidFactory;

/**
 * Value object for article id.
 */
class ArticleId
{
    /**
     * @var string
     */
    private $id;

    /**
     * @param string $id
     *
     * @throws InvalidArticleIdException
     */
    public function __construct(string $id)
    {
        $uuidFactory = new UuidFactory();

        try {
            $uuidFactory->fromString($id);
        } catch (\Exception $exception) {
            throw new InvalidArticleIdException($id);
        }

        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId() : string
    {
        return $this->id;
    }

    /**
     * Returns whether two values are equal.
     *
     * @param ArticleId $other
     *
     * @return bool
     */
    public function equals(ArticleId $other) : bool
    {
        return $this->id === $other->getId();
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->id;
    }
}
