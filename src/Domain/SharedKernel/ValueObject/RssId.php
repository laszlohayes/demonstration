<?php

declare(strict_types=1);

namespace Parser\Domain\SharedKernel\ValueObject;

use Parser\Domain\SharedKernel\Exception\InvalidRssIdException;
use Ramsey\Uuid\UuidFactory;

/**
 * Value object for rss id.
 */
class RssId
{
    /**
     * @var string
     */
    private $id;

    /**
     * @param string $id
     *
     * @throws InvalidRssIdException
     */
    public function __construct(string $id)
    {
        $uuidFactory = new UuidFactory();

        try {
            $uuidFactory->fromString($id);
        } catch (\Exception $exception) {
            throw new InvalidRssIdException($id);
        }

        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId() : string
    {
        return $this->id;
    }

    /**
     * Returns whether two values are equal.
     *
     * @param RssId $other
     *
     * @return bool
     */
    public function equals(RssId $other) : bool
    {
        return $this->id === $other->getId();
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->id;
    }
}
