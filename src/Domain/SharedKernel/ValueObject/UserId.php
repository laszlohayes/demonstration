<?php

declare(strict_types=1);

namespace Parser\Domain\SharedKernel\ValueObject;

use Parser\Domain\SharedKernel\Exception\InvalidUserIdException;
use Ramsey\Uuid\UuidFactory;

/**
 * Value object for user id.
 */
class UserId
{
    /**
     * @var string
     */
    private $id;

    /**
     * @param string $id
     *
     * @throws InvalidUserIdException
     */
    public function __construct(string $id)
    {
        $uuidFactory = new UuidFactory();

        try {
            $uuidFactory->fromString($id);
        } catch (\Exception $exception) {
            throw new InvalidUserIdException($id);
        }

        $this->id = $id;
    }

    /**
     * {@inheritdoc}
     */
    public function getId() : string
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function equals(UserId $other) : bool
    {
        return $this->id === $other->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function __toString() : string
    {
        return $this->id;
    }
}
