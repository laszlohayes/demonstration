<?php

declare(strict_types=1);

namespace Parser\Domain\SharedKernel\Dictionary;

/**
 * Class for source.
 */
class SourceType
{
    const HABR = 'habr';
}
