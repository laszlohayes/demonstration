<?php

declare(strict_types=1);

namespace Parser\Domain\SharedKernel\Exception;

/**
 * Exception for invalid rss id.
 */
class InvalidRssIdException extends \Exception implements DomainExceptionInterface
{
    /**
     * @var string
     */
    private $rssId;

    /**
     * @param string $rssId
     */
    public function __construct(string $rssId)
    {
        parent::__construct(sprintf('Invalid rss ID %s.', $rssId));

        $this->rssId = $rssId;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return 'f249fbf3-a8df-439f-8239-efa161608040';
    }

    /**
     * @return string
     */
    public function getRssId() : string
    {
        return $this->rssId;
    }
}
