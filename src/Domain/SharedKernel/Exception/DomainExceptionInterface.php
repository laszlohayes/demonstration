<?php

declare(strict_types=1);

namespace Parser\Domain\SharedKernel\Exception;

/**
 * Interface for domain exceptions.
 */
interface DomainExceptionInterface extends \Throwable
{
    /**
     * Gets error code as UUID4 hash.
     */
    public function getErrorCode() : string;
}
