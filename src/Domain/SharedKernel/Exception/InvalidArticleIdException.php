<?php

declare(strict_types=1);

namespace Parser\Domain\SharedKernel\Exception;

/**
 * Exception for invalid article id.
 */
class InvalidArticleIdException extends \Exception implements DomainExceptionInterface
{
    /**
     * @var string
     */
    private $articleId;

    /**
     * @param string $articleId
     */
    public function __construct(string $articleId)
    {
        parent::__construct(sprintf('Invalid article ID %s.', $articleId));

        $this->articleId = $articleId;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return '133614e6-1b4a-41e8-aa92-d3035ccf4156';
    }

    /**
     * @return string
     */
    public function getArticleId() : string
    {
        return $this->articleId;
    }
}
