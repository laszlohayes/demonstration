<?php

declare(strict_types=1);

namespace Parser\Domain\SharedKernel\Exception;

/**
 * Exception for invalid user id.
 */
class InvalidUserIdException extends \Exception implements DomainExceptionInterface
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @param string $userId
     */
    public function __construct(string $userId)
    {
        parent::__construct(
            sprintf(
                'Invalid user ID %s.',
                $userId
            )
        );

        $this->userId = $userId;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return 'de6af21a-fe03-477d-a2c3-02da4cdba35a';
    }

    /**
     * @return string
     */
    public function getClientId() : string
    {
        return $this->userId;
    }
}
