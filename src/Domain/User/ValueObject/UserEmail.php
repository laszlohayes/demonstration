<?php

declare(strict_types=1);

namespace Parser\Domain\User\ValueObject;

use Parser\Domain\User\Exception\InvalidUserEmailException;

/**
 * Value object for user email.
 */
class UserEmail
{
    /**
     * @var string
     */
    private $email;

    /**
     * @param string $email
     *
     * @throws InvalidUserEmailException
     */
    public function __construct(string $email)
    {
        if (strlen(trim($email)) === 0) {
            throw new InvalidUserEmailException($email);
        }

        $this->email = $email;
    }

    /**
     * Returns whether two values are equal.
     *
     * @param UserEmail $other
     *
     * @return bool
     */
    public function equals(UserEmail $other) : bool
    {
        return $this->email === $other->getEmail();
    }

    /**
     * @return string
     */
    public function getEmail() : string 
    {
        return $this->email;
    }
}
