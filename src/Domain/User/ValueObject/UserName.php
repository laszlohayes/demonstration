<?php

declare(strict_types=1);

namespace Parser\Domain\User\ValueObject;

use Parser\Domain\User\Exception\InvalidUserNameException;

/**
 * Value object for user name.
 */
class UserName
{
    /**
     * @var string
     */
    private $name;

    /**
     * @param string $name
     *
     * @throws InvalidUserNameException
     */
    public function __construct(string $name)
    {
        if (strlen(trim($name)) === 0) {
            throw new InvalidUserNameException($name);
        }

        $this->name = $name;
    }

    /**
     * Returns whether two values are equal.
     *
     * @param UserName $other
     *
     * @return bool
     */
    public function equals(UserName $other) : bool
    {
        return $this->name === $other->getName();
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
}
