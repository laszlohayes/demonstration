<?php

declare(strict_types=1);

namespace Parser\Domain\User\Model;

use Parser\Domain\Article\ValueObject\Title;
use Parser\Domain\SharedKernel\ValueObject\ArticleId;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Representation of article in user model.
 */
class Article
{
    /**
     * @var ArticleId
     */
    private $id;

    /**
     * @var Title
     */
    private $title;

    /**
     * @return ArticleId
     */
    public function getId() : ArticleId
    {
        return $this->id;
    }

    /**
     * @return Title
     */
    public function getTitle() : Title
    {
        return $this->title;
    }
}
