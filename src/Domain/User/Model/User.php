<?php

declare(strict_types=1);

namespace Parser\Domain\User\Model;

use Parser\Domain\SharedKernel\ValueObject\ArticleId;
use Parser\Domain\SharedKernel\ValueObject\RssId;
use Parser\Domain\SharedKernel\ValueObject\UserId;
use Parser\Domain\User\Exception\ArticleAlreadyExistsException;
use Parser\Domain\User\Exception\ArticleNotFoundException;
use Parser\Domain\User\Exception\RssAlreadyExistsException;
use Parser\Domain\User\Exception\RssNotFoundException;
use Parser\Domain\User\ValueObject\UserEmail;
use Parser\Domain\User\ValueObject\UserName;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User model.
 */
class User
{
    /**
     * @var UserId
     */
    protected $id;

    /**
     * @var UserName
     */
    private $name;

    /**
     * @var UserEmail
     */
    private $email;

    /**
     * @var Article[]
     */
    private $articles;

    /**
     * @var Rss[]
     */
    private $rss;

    /**
     * @param UserId    $id
     * @param UserName  $name
     * @param UserEmail $email
     */
    public function __construct(UserId $id, UserName $name, UserEmail $email)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->articles = new ArrayCollection();
        $this->rss = new ArrayCollection();
    }

    /**
     * @return UserId
     */
    public function getId() : UserId
    {
        return $this->id;
    }

    /**
     * @return UserName
     */
    public function getName() : UserName
    {
        return $this->name;
    }

    /**
     * @return UserEmail
     */
    public function getEmail() : UserEmail
    {
        return $this->email;
    }

    /**
     * @return Article[]
     */
    public function getArticles() : array
    {
        return $this->articles->toArray();
    }

    /**
     * Adds article.
     *
     * @param Article $article
     *
     * @return User
     * @throws ArticleAlreadyExistsException
     */
    public function addArticles(Article $article) : User
    {
        try {
            $this->findArticleById($article->getId());

            throw new ArticleAlreadyExistsException($article->getId()->getId());
        } catch (ArticleNotFoundException $exception) {
            $this->articles->add($article);
        }

        return $this;
    }

    /**
     * Returns article by id.
     *
     * @param ArticleId $id
     *
     * @return Article
     * @throws ArticleNotFoundException
     */
    public function findArticleById(ArticleId $id) : Article
    {
        $article = $this->articles->filter(function (Article $article) use ($id) {
            return $article->getId()->equals($id);
        })->first();

        if (!$article) {
            throw new ArticleNotFoundException($id->getId());
        }

        return $article;
    }


    /**
     * @return Rss[]
     */
    public function getRss() : array
    {
        return $this->rss->toArray();
    }

    /**
     * Adds rss.
     *
     * @param Rss $rss
     *
     * @return User
     * @throws RssAlreadyExistsException
     */
    public function addRss(Rss $rss) : User
    {
        try {
            $this->findRssById($rss->getId());

            throw new RssAlreadyExistsException($rss->getId()->getId());
        } catch (RssNotFoundException $exception) {
            $this->rss->add($rss);
        }

        return $this;
    }

    /**
     * Returns rss by id.
     *
     * @param RssId $id
     *
     * @return Rss
     * @throws RssNotFoundException
     */
    public function findRssById(RssId $id) : Rss
    {
        $rss = $this->rss->filter(function (Rss $rss) use ($id) {
            return $rss->getId()->equals($id);
        })->first();

        if (!$rss) {
            throw new RssNotFoundException($id->getId());
        }

        return $rss;
    }
}
