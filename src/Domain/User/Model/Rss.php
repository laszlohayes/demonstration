<?php

declare(strict_types=1);

namespace Parser\Domain\User\Model;

use Parser\Domain\Rss\ValueObject\Title;
use Parser\Domain\SharedKernel\ValueObject\RssId;

/**
 * Representation of rss in user model.
 */
class Rss
{
    /**
     * @var RssId
     */
    private $id;

    /**
     * @var Title
     */
    private $title;

    /**
     * @return RssId
     */
    public function getId() : RssId
    {
        return $this->id;
    }

    /**
     * @return Title
     */
    public function getTitle() : Title
    {
        return $this->title;
    }
}
