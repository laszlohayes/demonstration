<?php

declare(strict_types=1);

namespace Parser\Domain\User\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception for not found user.
 */
class UserNotFoundException extends \Exception implements DomainExceptionInterface
{
    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        parent::__construct(sprintf("User '%s' not found.", $value));
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return '67926590-2fdf-4979-a619-b9acbc35eb21';
    }
}
