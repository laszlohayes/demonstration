<?php

declare(strict_types=1);

namespace Parser\Domain\User\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception for invalid user email.
 */
class InvalidUserEmailException extends \Exception implements DomainExceptionInterface
{
    /**
     * @var string
     */
    private $email;

    /**
     * @param string $email
     */
    public function __construct(string $email)
    {
        parent::__construct(sprintf('Invalid user email %s.', $email));

        $this->email = $email;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return '2a6f7039-60a4-48d7-92d0-c2ea4127539d';
    }

    /**
     * @return string
     */
    public function getUserEmail() : string
    {
        return $this->email;
    }
}
