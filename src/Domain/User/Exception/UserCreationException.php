<?php

declare(strict_types=1);

namespace Parser\Domain\User\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception while creating user.
 */
class UserCreationException extends \Exception implements DomainExceptionInterface
{
    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return '20e289fe-a225-42d8-8ddb-042ed5ce4cee';
    }
}
