<?php

declare(strict_types=1);

namespace Parser\Domain\User\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception while creating existing user.
 */
class UserAlreadyExistsException extends \Exception implements DomainExceptionInterface
{
    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        parent::__construct(sprintf('User already exists. Duplicated value %s', $value));
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return 'a2fe65d1-5571-483c-9ab7-ae279a61f9b6';
    }
}
