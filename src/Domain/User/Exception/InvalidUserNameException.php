<?php

declare(strict_types=1);

namespace Parser\Domain\User\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception for invalid user name.
 */
class InvalidUserNameException extends \Exception implements DomainExceptionInterface
{
    /**
     * @var string
     */
    private $userName;

    /**
     * @param string $userName
     */
    public function __construct(string $userName)
    {
        parent::__construct(sprintf('Invalid user name %s.', $userName));

        $this->userName = $userName;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return '2a6f7039-60a4-48d7-92d0-c2ea4127539d';
    }

    /**
     * @return string
     */
    public function getUserName() : string
    {
        return $this->userName;
    }
}
