<?php

declare(strict_types=1);

namespace Parser\Domain\User\Repository;

use Parser\Domain\SharedKernel\ValueObject\UserId;
use Parser\Domain\User\ValueObject\UserEmail;
use Parser\Domain\User\Exception\UserNotFoundException;
use Parser\Domain\User\Model\User;

/**
 * Interface for user repository.
 */
interface UserRepositoryInterface
{
    /**
     * Returns user model.
     *
     * @param UserId $id
     *
     * @return User
     * @throws UserNotFoundException
     */
    public function ofId(UserId $id) : User;

    /**
     * Returns user model by email.
     *
     * @param UserEmail $email
     *
     * @return User
     * @throws UserNotFoundException
     */
    public function byEmail(UserEmail $email) : User;

    /**
     * Saves model.
     *
     * @param User $user
     */
    public function save(User $user) : void;
}
