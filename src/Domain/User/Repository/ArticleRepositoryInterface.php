<?php

declare(strict_types=1);

namespace Parser\Domain\User\Repository;

use Parser\Domain\User\Exception\ArticleNotFoundException;
use Parser\Domain\User\Model\Article;
use Parser\Domain\SharedKernel\ValueObject\ArticleId;

/**
 * Interface for article repository.
 */
interface ArticleRepositoryInterface
{
    /**
     * Returns article model.
     *
     * @param ArticleId $id
     *
     * @return Article
     * @throws ArticleNotFoundException
     */
    public function ofId(ArticleId $id) : Article;
}
