<?php

declare(strict_types=1);

namespace Parser\Domain\User\Repository;

use Parser\Domain\User\Exception\RssNotFoundException;
use Parser\Domain\User\Model\Rss;
use Parser\Domain\SharedKernel\ValueObject\RssId;

/**
 * Interface for rss repository.
 */
interface RssRepositoryInterface
{
    /**
     * Returns rss model.
     *
     * @param RssId $id
     *
     * @return Rss
     * @throws RssNotFoundException
     */
    public function ofId(RssId $id) : Rss;
}
