<?php

declare(strict_types=1);

namespace Parser\Domain\Article\Model;

use Parser\Domain\Article\ValueObject\Description;
use Parser\Domain\Article\ValueObject\Url;
use Parser\Domain\Article\ValueObject\Tag;
use Parser\Domain\Article\ValueObject\Title;
use Parser\Domain\SharedKernel\ValueObject\ArticleId;
use Parser\Domain\SharedKernel\ValueObject\UserId;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Article aggregate root.
 */
class Article
{
    /**
     * @var ArticleId
     */
    private $id;

    /**
     * @var Title
     */
    private $title;

    /**
     * @var Url
     */
    private $url;

    /**
     * @var Description
     */
    private $description;

    /**
     * @var Tag[]
     */
    private $tags;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param ArticleId   $id
     * @param Title       $title
     * @param Url         $url
     * @param Description $description
     * @param Tag[]       $tags
     * @param UserId      $userId
     */
    public function __construct(
        ArticleId $id,
        Title $title,
        Url $url,
        Description $description,
        ?array $tags,
        UserId $userId
    )
    {
        $this->id = $id;
        $this->title = $title;
        $this->url = $url;
        $this->description = $description;
        $this->tags = new ArrayCollection($tags);
        $this->userId = $userId;
    }

    /**
     * @return ArticleId
     */
    public function getId() : ArticleId
    {
        return $this->id;
    }

    /**
     * @return Title
     */
    public function getTitle() : Title
    {
        return $this->title;
    }

    /**
     * @return Url
     */
    public function getUrl() : Url
    {
        return $this->url;
    }

    /**
     * @return Description
     */
    public function getDescription() : Description
    {
        return $this->description;
    }

    /**
     * @return Tag[]
     */
    public function getTags() : array
    {
        return $this->tags;
    }

    /**
     * @return UserId
     */
    public function getUserId() : UserId
    {
        return $this->userId;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt() : \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function touch()
    {
        if (!$this->createdAt) {
            $this->createdAt = new \DateTimeImmutable();
        }
    }
}
