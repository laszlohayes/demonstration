<?php

declare(strict_types=1);

namespace Parser\Domain\Article\Repository;

use Parser\Domain\Article\Exception\ArticleNotFoundException;
use Parser\Domain\Article\Model\Article;
use Parser\Domain\Article\ValueObject\Url;
use Parser\Domain\SharedKernel\ValueObject\ArticleId;
use Parser\Domain\SharedKernel\ValueObject\UserId;

/**
 * Interface for article repository.
 */
interface ArticleRepositoryInterface
{
    /**
     * Returns article model.
     *
     * @param ArticleId $id
     *
     * @return Article
     * @throws ArticleNotFoundException
     */
    public function ofId(ArticleId $id) : Article;

    /**
     * Returns article model.
     *
     * @param Url $url
     *
     * @return Article
     * @throws ArticleNotFoundException
     */
    public function findByUrl(Url $url) : Article;

    /**
     * Returns article model.
     *
     * @param UserId $userId
     *
     * @return Article
     * @throws ArticleNotFoundException
     */
    public function findByUser(UserId $userId) : Article;

    /**
     * Saves model.
     *
     * @param Article $article
     */
    public function save(Article $article) : void;

    /**
     * Returns articles list.
     *
     * @return array
     */
    public function findAll() : array;
}
