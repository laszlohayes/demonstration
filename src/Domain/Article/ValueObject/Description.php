<?php

declare(strict_types=1);

namespace Parser\Domain\Article\ValueObject;

use Parser\Domain\Article\Exception\InvalidDescriptionException;

/**
 * Value object for article description.
 */
class Description
{
    /**
     * @var string
     */
    private $description;

    /**
     * @param string $description
     *
     * @throws InvalidDescriptionException
     */
    public function __construct(string $description)
    {
        if (strlen(trim($description)) === 0) {
            throw new InvalidDescriptionException($description);
        }

        $this->description = $description;
    }

    /**
     * Returns whether two values are equal.
     *
     * @param Description $other
     *
     * @return bool
     */
    public function equals(Description $other) : bool
    {
        return $this->description === $other->getDescription();
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }
}
