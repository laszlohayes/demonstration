<?php

declare(strict_types=1);

namespace Parser\Domain\Article\ValueObject;

use Parser\Domain\Article\Exception\InvalidUrlException;

/**
 * Value object for article url.
 */
class Url
{
    /**
     * @var string
     */
    private $url;

    /**
     * @param string $url
     *
     * @throws InvalidUrlException
     */
    public function __construct(string $url)
    {
        if (strlen(trim($url)) === 0) {
            throw new InvalidUrlException($url);
        }

        $this->url = $url;
    }

    /**
     * Returns whether two values are equal.
     *
     * @param Url $other
     *
     * @return bool
     */
    public function equals(Url $other) : bool
    {
        return $this->url === $other->getUrl();
    }

    /**
     * @return string
     */
    public function getUrl() : string
    {
        return $this->url;
    }
}
