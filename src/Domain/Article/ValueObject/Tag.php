<?php

declare(strict_types=1);

namespace Parser\Domain\Article\ValueObject;

use Parser\Domain\Article\Exception\InvalidTagException;

/**
 * Value object for article tag.
 */
class Tag
{
    /**
     * @var string
     */
    private $tag;

    /**
     * @param string $tag
     *
     * @throws InvalidTagException
     */
    public function __construct(string $tag)
    {
        if (strlen(trim($tag)) === 0) {
            throw new InvalidTagException($tag);
        }

        $this->tag = $tag;
    }

    /**
     * Returns whether two values are equal.
     *
     * @param Tag $other
     *
     * @return bool
     */
    public function equals(Tag $other) : bool
    {
        return $this->tag === $other->getTag();
    }

    /**
     * @return string
     */
    public function getTag() : string
    {
        return $this->tag;
    }
}
