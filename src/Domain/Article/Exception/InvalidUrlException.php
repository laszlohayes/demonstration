<?php

declare(strict_types=1);

namespace Parser\Domain\Article\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception for invalid url.
 */
class InvalidUrlException extends \Exception implements DomainExceptionInterface
{
    /**
     * @var string
     */
    private $url;

    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        parent::__construct(sprintf('Invalid url %s.', $value));

        $this->url = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return 'b55c5b8b-5bea-4646-a84a-d9876efcefdc';
    }

    /**
     * @return string
     */
    public function getUrl() : string
    {
        return $this->url;
    }
}
