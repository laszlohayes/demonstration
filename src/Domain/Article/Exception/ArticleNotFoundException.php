<?php

declare(strict_types=1);

namespace Parser\Domain\Article\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception while client not found.
 */
class ArticleNotFoundException extends \Exception implements DomainExceptionInterface
{
    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        parent::__construct(sprintf('Article with value %s not found.', $value));
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return '36491575-f616-49de-a143-c17a264333a8';
    }
}
