<?php

declare(strict_types=1);

namespace Parser\Domain\Article\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception while creating existing article.
 */
class ArticleAlreadyExistsException extends \Exception implements DomainExceptionInterface
{
    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        parent::__construct(sprintf('Article already exists. Duplicated value %s', $value));
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return 'e578d182-dd43-4453-af52-a67fe98f4f80';
    }
}
