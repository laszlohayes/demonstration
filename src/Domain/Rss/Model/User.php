<?php

declare(strict_types=1);

namespace Parser\Domain\Rss\Model;

use Parser\Domain\Rss\ValueObject\UserName;
use Parser\Domain\SharedKernel\ValueObject\UserId;

/**
 * Representation of user in rss model.
 */
class User
{
    /**
     * @var UserId
     */
    private $id;

    /**
     * @var UserName
     */
    private $name;

    /**
     * @return UserId
     */
    public function getId() : UserId
    {
        return $this->id;
    }

    /**
     * @return UserName
     */
    public function getName() : UserName
    {
        return $this->name;
    }
}
