<?php

declare(strict_types=1);

namespace Parser\Domain\Rss\Model;

use Parser\Domain\Rss\ValueObject\Source;
use Parser\Domain\Rss\ValueObject\Tag;
use Parser\Domain\Rss\ValueObject\Title;
use Parser\Domain\Rss\ValueObject\Url;
use Parser\Domain\SharedKernel\ValueObject\RssId;
use Doctrine\Common\Collections\ArrayCollection;
use Parser\Domain\SharedKernel\ValueObject\UserId;

/**
 * Aggregation root for rss.
 */
class Rss
{
    /**
     * @var RssId
     */
    private $id;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var Source
     */
    private $source;

    /**
     * @var Title
     */
    private $title;

    /**
     * @var Url
     */
    private $url;

    /**
     * @var Tag[]
     */
    private $tags;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param RssId  $id
     * @param UserId $userId
     * @param Source $source
     * @param Title  $title
     * @param Url    $url
     * @param Tag[]  $tags
     */
    public function __construct(
        RssId $id,
        UserId $userId,
        Source $source,
        Title $title,
        Url $url,
        array $tags
    )
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->source = $source;
        $this->title = $title;
        $this->url = $url;
        $this->tags = new ArrayCollection($tags);
    }

    /**
     * @return RssId
     */
    public function getId() : RssId
    {
        return $this->id;
    }

    /**
     * @return UserId
     */
    public function getUserId() : UserId
    {
        return $this->userId;
    }

    /**
     * @return Source
     */
    public function getSource() : Source
    {
        return $this->source;
    }

    /**
     * @return Title
     */
    public function getTitle() : Title
    {
        return $this->title;
    }

    /**
     * @return Url
     */
    public function getUrl() : Url
    {
        return $this->url;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt() : \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param Tag $tag
     */
    public function addTag(Tag $tag) : void
    {
        $this->tags[] = $tag;
    }

    /**
     * @return Tag[]
     */
    public function getTags() : array
    {
        return $this->tags->toArray();
    }

    public function touch()
    {
        if (!$this->createdAt) {
            $this->createdAt = new \DateTimeImmutable();
        }
    }

    /**
     * @param Title $title
     */
    public function setTitle(Title $title) : void
    {
        $this->title = $title;
    }

    /**
     * @param Url $url
     */
    public function setUrl(Url $url) : void
    {
        $this->url = $url;
    }

    /**
     * @param Tag[] $tags
     */
    public function setTags(array $tags) : void
    {
        $this->tags = new ArrayCollection($tags);
    }
}
