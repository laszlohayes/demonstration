<?php

declare(strict_types=1);

namespace Parser\Domain\Rss\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception for invalid description.
 */
class InvalidDescriptionException extends \Exception implements DomainExceptionInterface
{
    /**
     * @var string
     */
    private $description;

    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        parent::__construct(sprintf('Invalid description %s.', $value));

        $this->description = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return 'b55c5b8b-5bea-4646-a84a-d9876efcefdc';
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }
}
