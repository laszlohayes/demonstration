<?php

declare(strict_types=1);

namespace Parser\Domain\Rss\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception while adding existing rss channel.
 */
class RssAlreadyExistsException extends \Exception implements DomainExceptionInterface
{
    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        parent::__construct(sprintf('Rss already exists. Duplicated value %s', $value));
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return 'ab0c1807-8ac3-41a2-98d2-eae3a8a58991';
    }
}
