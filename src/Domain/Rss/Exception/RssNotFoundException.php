<?php

declare(strict_types=1);

namespace Parser\Domain\Rss\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception when can't find the rss.
 */
class RssNotFoundException extends \Exception implements DomainExceptionInterface
{
    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        parent::__construct(sprintf('Rss with value %s not found.', $value));
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return 'f773c2a7-749e-4a5d-9bff-450ac6d05a58';
    }
}
