<?php

declare(strict_types=1);

namespace Parser\Domain\Rss\Exception;

use Parser\Domain\SharedKernel\Exception\DomainExceptionInterface;

/**
 * Exception for invalid source.
 */
class InvalidSourceException extends \Exception implements DomainExceptionInterface
{
    /**
     * @var string
     */
    private $source;

    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        parent::__construct(sprintf('Invalid source %s.', $value));

        $this->source = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorCode() : string
    {
        return '5afb9e60-6999-4a2a-a740-2e11dfbade2d';
    }

    /**
     * @return string
     */
    public function getSource() : string
    {
        return $this->source;
    }
}
