<?php

declare(strict_types=1);

namespace Parser\Domain\Rss\Repository;

use Parser\Domain\Rss\Exception\RssNotFoundException;
use Parser\Domain\Rss\Model\Rss;
use Parser\Domain\Rss\ValueObject\Url;
use Parser\Domain\SharedKernel\ValueObject\RssId;
use Parser\Domain\SharedKernel\ValueObject\UserId;

/**
 * Interface for rss repository.
 */
interface RssRepositoryInterface
{
    /**
     * Returns rss model.
     *
     * @param RssId $id
     *
     * @return Rss
     * @throws RssNotFoundException
     */
    public function ofId(RssId $id) : Rss;

    /**
     * Returns rss model.
     *
     * @param Url $url
     *
     * @return Rss
     * @throws RssNotFoundException
     */
    public function findByUrl(Url $url) : Rss;

    /**
     * Returns rss model.
     *
     * @param UserId $userId
     *
     * @return Rss
     * @throws RssNotFoundException
     */
    public function findByUser(UserId $userId) : Rss;

    /**
     * Saves model.
     *
     * @param Rss $rss
     */
    public function save(Rss $rss) : void;

    /**
     * Deletes model.
     *
     * @param Rss $rss
     */
    public function delete(Rss $rss) : void;

    /**
     * Returns rss list.
     *
     * @return Rss[]
     */
    public function findAll() : array;
}
