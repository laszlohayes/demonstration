<?php

declare(strict_types=1);

namespace Parser\Domain\Rss\ValueObject;

use Parser\Domain\Rss\Exception\InvalidSourceException;

/**
 * Value object for rss source.
 */
class Source
{
    /**
     * @var string
     */
    private $source;

    /**
     * @param string $source
     *
     * @throws InvalidSourceException
     */
    public function __construct(string $source)
    {
        if (strlen(trim($source)) === 0) {
            throw new InvalidSourceException($source);
        }

        $this->source = $source;
    }

    /**
     * Returns whether two values are equal.
     *
     * @param Source $other
     *
     * @return bool
     */
    public function equals(Source $other) : bool
    {
        return $this->source === $other->getSource();
    }

    /**
     * @return string
     */
    public function getSource() : string
    {
        return $this->source;
    }
}
