<?php

declare(strict_types=1);

namespace Parser\Domain\Rss\ValueObject;

use Parser\Domain\Rss\Exception\InvalidTitleException;

/**
 * Value object for rss title.
 */
class Title
{
    /**
     * @var string
     */
    private $title;

    /**
     * @param string $title
     *
     * @throws InvalidTitleException
     */
    public function __construct(string $title)
    {
        if (strlen(trim($title)) === 0) {
            throw new InvalidTitleException($title);
        }

        $this->title = $title;
    }

    /**
     * Returns whether two values are equal.
     *
     * @param Title $other
     *
     * @return bool
     */
    public function equals(Title $other) : bool
    {
        return $this->title === $other->getTitle();
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }
}
