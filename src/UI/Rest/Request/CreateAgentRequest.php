<?php

declare(strict_types=1);

namespace Parser\UI\Rest\Request;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * DTO for users create.
 */
class CreateAgentRequest
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    public $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    public $name;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    public $email;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    public $password;
}
