<?php

declare(strict_types=1);

namespace Parser\UI\Rest\Request;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * DTO for rss update.
 */
class UpdateRssRequest
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    public $name;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    public $url;

    /**
     * @var array
     *
     * @Assert\NotBlank()
     * @Serializer\Type("array")
     */
    public $tags;
}
