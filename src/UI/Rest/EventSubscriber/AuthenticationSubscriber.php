<?php

declare(strict_types=1);

namespace Parser\UI\Rest\EventSubscriber;

use Parser\Application\Query\AuthQuery;
use Parser\Application\Response\AuthResponse;
use Parser\Domain\Auth\Exception\UserNotFoundException;
use Doctrine\Common\Annotations\Reader;
use Parser\Infrastructure\Security\Annotation\Anonymous;
use Xiglow\ApplicationBundle\QueryHandling\Interfaces\QueryBusInterface;
use Parser\Infrastructure\Security\AuthData;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Authentication kernel subscriber.
 */
class AuthenticationSubscriber implements EventSubscriberInterface
{
    /**
     * @var QueryBusInterface
     */
    private $queryBus;

    /**
     * @var Reader
     */
    private $reader;

    /**
     * @param QueryBusInterface $queryBus
     * @param Reader            $reader
     */
    public function __construct(QueryBusInterface $queryBus, Reader $reader)
    {
        $this->queryBus = $queryBus;
        $this->reader = $reader;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['authenticateApiKey', 0],
            ],
            KernelEvents::CONTROLLER => [
                ['processSecurity', 0],
            ],
        ];
    }

    /**
     * Processes API key authentication.
     *
     * @param GetResponseEvent $event
     */
    public function authenticateApiKey(GetResponseEvent $event) : void
    {
        $apiKey = $event->getRequest()->headers->get('x-api-key');

        try {
            if (!$apiKey) {
                throw new UserNotFoundException('');
            }

            /** @var AuthResponse $user */
            $user = $this->queryBus->query(new AuthQuery($apiKey));

            $authData = new AuthData($user->userId, null);
        } catch (UserNotFoundException $exception) {
            $authData = null;
        }

        $event->getRequest()->attributes->set('authData', $authData);
    }

    /**
     * Processes controller security.
     *
     * @param FilterControllerEvent $event
     */
    public function processSecurity(FilterControllerEvent $event) : void
    {
        $controller = $event->getController();
        $action = (new \ReflectionObject($controller[0]))->getMethod($controller[1]);

        /** @var AuthData $authData */
        $authData = $event->getRequest()->attributes->get('authData');

        /** @var Anonymous $anonymous */
        $anonymous = $this->reader->getMethodAnnotation($action, Anonymous::class);
        if (!$authData && !$anonymous) {
            throw new UnauthorizedHttpException('');
        }
    }
}
