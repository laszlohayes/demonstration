<?php

declare(strict_types=1);

namespace Parser\UI\Rest\Controller;

use Parser\Application\Query\LoginQuery;
use Parser\UI\Rest\Request\LoginRequest;
use FOS\RestBundle\Controller\Annotations as Rest;
use Xiglow\RestBundle\Component\Interfaces\RestComponentInterface;
use Symfony\Component\HttpFoundation\Response;
use Parser\Infrastructure\Security\Annotation as Security;

/**
 * Auth API controller.
 *
 * @Rest\Route("/authenticate")
 */
class AuthController
{
    /**
     * @var RestComponentInterface
     */
    private $restComponent;

    /**
     * @param RestComponentInterface $restComponent
     */
    public function __construct(RestComponentInterface $restComponent)
    {
        $this->restComponent = $restComponent;
    }

    /**
     * Returns apiKey for user by credentials.
     *
     * @param LoginRequest $request
     *
     * @return Response
     *
     * @Security\Anonymous()
     *
     * @Rest\Post()
     */
    public function loginAction(LoginRequest $request) : Response
    {
        $result = $this->restComponent->query(new LoginQuery(
            $request->email,
            $request->password
        ));

        return $this->restComponent->serialize($result);
    }
}
