<?php

declare(strict_types=1);

namespace Parser\UI\Rest\Controller;

use Parser\Application\Command\CreateRssCommand;
use Parser\Application\Command\DeleteRssCommand;
use Parser\Application\Command\UpdateRssCommand;
use Parser\Application\Query\GetRssListQuery;
use Parser\UI\Rest\Request\CreateRssRequest;
use Parser\UI\Rest\Request\UpdateRssRequest;
use FOS\RestBundle\Controller\Annotations as Rest;
use Xiglow\RestBundle\Component\Interfaces\RestComponentInterface;
use Parser\Infrastructure\Security\AuthData;
use Symfony\Component\HttpFoundation\Response;

/**
 * REST controller for rss.
 *
 * @Rest\Route("/rss")
 */
class RssController
{
    /**
     * @var RestComponentInterface
     */
    private $restComponent;

    /**
     * @param RestComponentInterface $restComponent
     */
    public function __construct(RestComponentInterface $restComponent)
    {
        $this->restComponent = $restComponent;
    }

    /**
     * Creates new rss.
     *
     * @param CreateRssRequest $createRssRequest
     * @param AuthData         $authData
     *
     * @return Response
     *
     * @Rest\Post()
     */
    public function createAction(CreateRssRequest $createRssRequest, AuthData $authData) : Response
    {
        $this->restComponent->handle(
            new CreateRssCommand(
                $authData->getUserId(),
                $createRssRequest->id,
                $createRssRequest->name,
                $createRssRequest->url,
                $createRssRequest->tags
            ));

        return new Response();
    }

    /**
     * Updates rss.
     *
     * @param string           $id
     * @param UpdateRssRequest $updateRssRequest
     * @param AuthData         $authData
     *
     * @return Response
     *
     * @Rest\Patch("/{id}")
     */
    public function updateAction(string $id, UpdateRssRequest $updateRssRequest, AuthData $authData) : Response
    {
        $this->restComponent->handle(
            new UpdateRssCommand(
                $authData->getUserId(),
                $id,
                $updateRssRequest->name,
                $updateRssRequest->url,
                $updateRssRequest->tags
            ));

        return new Response();
    }

    /**
     * Deletes rss.
     *
     * @param string   $id
     * @param AuthData $authData
     *
     * @return Response
     *
     * @Rest\Delete("/{id}")
     */
    public function deleteAction(string $id, AuthData $authData) : Response
    {
        $this->restComponent->handle(new DeleteRssCommand($id, $authData->getUserId()));

        return new Response();
    }

    /**
     * Returns rss list.
     *
     * @param AuthData $authData
     *
     * @return Response
     *
     * @Rest\Get()
     */
    public function listAction(AuthData $authData) : Response
    {
        $result = $this->restComponent->query(new GetRssListQuery($authData->getUserId()));

        return $this->restComponent->serialize($result);
    }
}
