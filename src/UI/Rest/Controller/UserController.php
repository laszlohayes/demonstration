<?php

declare(strict_types=1);

namespace Parser\UI\Rest\Controller;

use Parser\Application\Command\ParseCommand;
use Parser\Application\Query\GetUserQuery;
use Xiglow\RestBundle\Component\Interfaces\RestComponentInterface;
use Parser\Infrastructure\Security\AuthData;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * User API controller.
 *
 * @Rest\Route("/users")
 */
class UserController
{
    /**
     * @var RestComponentInterface
     */
    private $restComponent;

    /**
     * @param RestComponentInterface $restComponent
     */
    public function __construct(RestComponentInterface $restComponent)
    {
        $this->restComponent = $restComponent;
    }

    /**
     * Returns current user profile.
     *
     * @param AuthData $authData
     *
     * @return Response
     *
     * @Rest\Get("/me")
     */
    public function meAction(AuthData $authData) : Response
    {
        $result = $this->restComponent->handle(new ParseCommand());

        return $this->restComponent->serialize($result);
    }
}
