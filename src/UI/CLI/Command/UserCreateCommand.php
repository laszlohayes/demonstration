<?php

declare(strict_types=1);

namespace Parser\UI\CLI\Command;

use Parser\Application\Command\CreateUserCommand;
use Ramsey\Uuid\Uuid;
use Xiglow\ApplicationBundle\CommandHandling\Interfaces\CommandBusInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Command for creating user.
 */
class UserCreateCommand extends Command
{
    /**
     * @var CommandBusInterface
     */
    private $commandBus;

    /**
     * @param CommandBusInterface $commandBus
     */
    public function __construct(CommandBusInterface $commandBus)
    {
        parent::__construct('user:create');

        $this->setDescription('Creates user.');
        $this->commandBus = $commandBus;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');

        $nameQuestion = new Question('<comment>Enter user name: </comment>');
        $name = $helper->ask($input, $output, $nameQuestion);

        $emailQuestion = new Question('<comment>Enter email: </comment>');
        $email = $helper->ask($input, $output, $emailQuestion);

        $passwordQuestion = new Question('<comment>Enter password: </comment>');
        $password = $helper->ask($input, $output, $passwordQuestion);

        $question = new ConfirmationQuestion('<question>Create user with this credentials?(y/n)</question>', false);

        if (!$helper->ask($input, $output, $question)) {
            return;
        }

        $this->commandBus->dispatch(new CreateUserCommand(Uuid::uuid4()->toString(), $name, $email, $password));

        $output->writeln('<info>User created.</info>');
    }
}
