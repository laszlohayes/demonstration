<?php

declare(strict_types=1);

namespace Parser\UI\CLI\Command;

use Parser\Application\Command\ParseCommand;
use Xiglow\ApplicationBundle\CommandHandling\Interfaces\CommandBusInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command for parsing rss.
 */
class ParseRssCommand extends Command
{
    /**
     * @var CommandBusInterface
     */
    private $commandBus;

    /**
     * @param CommandBusInterface $commandBus
     */
    public function __construct(CommandBusInterface $commandBus)
    {
        parent::__construct('parse:rss');
        $this->setDescription('Starts parsing process.');

        $this->commandBus = $commandBus;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->commandBus->dispatch(new ParseCommand());
    }
}
