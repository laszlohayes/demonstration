<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Security;

/**
 * Security layer model.
 */
class AuthData
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $role;

    /**
     * @param string $userId
     * @param null|string $role
     */
    public function __construct(string $userId, ?string $role)
    {
        $this->userId = $userId;
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getUserId() : string
    {
        return $this->userId;
    }

    /**
     * @return null|string
     */
    public function getRole() : ?string
    {
        return $this->role;
    }
}
