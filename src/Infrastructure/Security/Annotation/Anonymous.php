<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Security\Annotation;

/**
 * Security controller annotation.
 *
 * @Annotation()
 * @Target({"CLASS", "METHOD"})
 */
class Anonymous
{
}
