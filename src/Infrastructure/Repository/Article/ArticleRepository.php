<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Repository\Article;

use Parser\Domain\Article\Exception\ArticleNotFoundException;
use Parser\Domain\Article\Model\Article;
use Parser\Domain\Article\Repository\ArticleRepositoryInterface;
use Parser\Domain\Article\ValueObject\Url;
use Parser\Domain\SharedKernel\ValueObject\ArticleId;
use Parser\Domain\SharedKernel\ValueObject\UserId;
use Xiglow\InfrastructureBundle\Component\Factory\Interfaces\RepositoryFactoryInterface;
use Xiglow\InfrastructureBundle\Component\Interfaces\RepositoryComponentInterface;
use Xiglow\InfrastructureBundle\Exception\ModelNotFoundException;

/**
 * Repository for article.
 */
class ArticleRepository implements ArticleRepositoryInterface
{
    /**
     * @var RepositoryComponentInterface
     */
    private $repositoryComponent;

    /**
     * @param RepositoryFactoryInterface $repositoryFactory
     */
    public function __construct(RepositoryFactoryInterface $repositoryFactory)
    {
        $this->repositoryComponent = $repositoryFactory->create(Article::class);
    }

    /**
     * {@inheritdoc}
     */
    public function ofId(ArticleId $id) : Article
    {
        try {
            return $this->repositoryComponent->find($id);
        } catch (ModelNotFoundException $exception) {
            throw new ArticleNotFoundException($id->getId());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findByUrl(Url $url) : Article
    {
        try {
            return $this->repositoryComponent->findOneBy(['url' => $url->getUrl()]);
        } catch (ModelNotFoundException $exception) {
            throw new ArticleNotFoundException($url->getUrl());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findByUser(UserId $userId) : Article
    {
        try {
            return $this->repositoryComponent->findOneBy(['user' => $userId->getId()]);
        } catch (ModelNotFoundException $exception) {
            throw new ArticleNotFoundException($userId->getId());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function save(Article $article) : void
    {
        $this->repositoryComponent->save($article);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll() : array
    {
        return $this->repositoryComponent->findBy([]);
    }
}
