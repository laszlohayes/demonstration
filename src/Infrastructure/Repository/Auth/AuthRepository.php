<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Repository\Auth;

use Parser\Domain\Auth\Exception\UserNotFoundException;
use Parser\Domain\Auth\Model\User;
use Parser\Domain\Auth\Repository\AuthRepositoryInterface;
use Parser\Domain\Auth\ValueObject\ApiKey;
use Parser\Domain\SharedKernel\ValueObject\UserId;
use Xiglow\InfrastructureBundle\Component\Factory\Interfaces\RepositoryFactoryInterface;
use Xiglow\InfrastructureBundle\Component\Interfaces\RepositoryComponentInterface;
use Xiglow\InfrastructureBundle\Exception\ModelNotFoundException;

/**
 * Repository for authentication.
 */
class AuthRepository implements AuthRepositoryInterface
{
    /**
     * @var RepositoryComponentInterface
     */
    private $repositoryComponent;

    /**
     * @param RepositoryFactoryInterface $repositoryFactory
     */
    public function __construct(RepositoryFactoryInterface $repositoryFactory)
    {
        $this->repositoryComponent = $repositoryFactory->create(User::class);
    }

    /**
     * {@inheritdoc}
     */
    public function ofId(UserId $id) : User
    {
        try {
            $user = $this->repositoryComponent->find($id);
        } catch (ModelNotFoundException $exception) {
            $user = new User($id);
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function save(User $user) : void
    {
        $this->repositoryComponent->save($user);
    }

    /**
     * {@inheritdoc}
     */
    public function byApiKey(ApiKey $apiKey) : User
    {
        $users = $this->repositoryComponent->findBy(['apiKey' => $apiKey->getApiKey()]);

        if (count($users) !== 1) {
            throw new UserNotFoundException($apiKey->getApiKey());
        }

        return reset($users);
    }
}
