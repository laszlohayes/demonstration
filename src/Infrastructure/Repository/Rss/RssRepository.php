<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Repository\Rss;

use Parser\Domain\Rss\Exception\RssNotFoundException;
use Parser\Domain\Rss\Model\Rss;
use Parser\Domain\Rss\Repository\RssRepositoryInterface;
use Parser\Domain\Rss\ValueObject\Url;
use Parser\Domain\SharedKernel\ValueObject\RssId;
use Parser\Domain\SharedKernel\ValueObject\UserId;
use Xiglow\InfrastructureBundle\Component\Factory\Interfaces\RepositoryFactoryInterface;
use Xiglow\InfrastructureBundle\Component\Interfaces\RepositoryComponentInterface;
use Xiglow\InfrastructureBundle\Exception\ModelNotFoundException;

/**
 * Repository for work item.
 */
class RssRepository implements RssRepositoryInterface
{
    /**
     * @var RepositoryComponentInterface
     */
    private $repositoryComponent;

    /**
     * @param RepositoryFactoryInterface $repositoryFactory
     */
    public function __construct(RepositoryFactoryInterface $repositoryFactory)
    {
        $this->repositoryComponent = $repositoryFactory->create(Rss::class);
    }

    /**
     * {@inheritdoc}
     *
     * @throws RssNotFoundException
     */
    public function ofId(RssId $id) : Rss
    {
        try {
            return $this->repositoryComponent->find($id);
        } catch (ModelNotFoundException $exception) {
            throw new RssNotFoundException($id->getId());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findByUrl(Url $url) : Rss
    {
        try {
            return $this->repositoryComponent->findOneBy(['url' => $url->getUrl()]);
        } catch (ModelNotFoundException $exception) {
            throw new RssNotFoundException($url->getUrl());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findByUser(UserId $userId) : Rss
    {
        try {
            return $this->repositoryComponent->findOneBy(['user' => $userId->getId()]);
        } catch (ModelNotFoundException $exception) {
            throw new RssNotFoundException($userId->getId());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function save(Rss $rss) : void
    {
        ;
        $this->repositoryComponent->save($rss);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(Rss $rss) : void
    {
        $this->repositoryComponent->delete($rss);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll() : array
    {
        return $this->repositoryComponent->findBy([]);
    }
}
