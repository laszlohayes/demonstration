<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Repository\User;

use Parser\Domain\SharedKernel\ValueObject\UserId;
use Parser\Domain\User\Exception\UserNotFoundException;
use Parser\Domain\User\Model\User;
use Parser\Domain\User\ValueObject\UserEmail;
use Parser\Domain\User\Repository\UserRepositoryInterface;
use Xiglow\InfrastructureBundle\Component\Factory\Interfaces\RepositoryFactoryInterface;
use Xiglow\InfrastructureBundle\Component\Interfaces\RepositoryComponentInterface;
use Xiglow\InfrastructureBundle\Exception\ModelNotFoundException;

/**
 * Repository for user.
 */
class UserRepository implements UserRepositoryInterface
{
    /**
     * @var RepositoryComponentInterface
     */
    private $repositoryComponent;

    /**
     * @param RepositoryFactoryInterface $repositoryFactory
     */
    public function __construct(RepositoryFactoryInterface $repositoryFactory)
    {
        $this->repositoryComponent = $repositoryFactory->create(User::class);
    }

    /**
     * {@inheritdoc}
     */
    public function ofId(UserId $id) : User
    {
        try {
            return $this->repositoryComponent->find($id);
        } catch (ModelNotFoundException $exception) {
            throw new UserNotFoundException($id->getId());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function byEmail(UserEmail $email) : User
    {
        $user = $this->repositoryComponent->findBy(['email' => $email->getEmail()]);

        if (!$user) {
            throw new UserNotFoundException($email->getEmail());
        }

        return reset($user);
    }

    /**
     * {@inheritdoc}
     */
    public function save(User $user) : void
    {
        $this->repositoryComponent->save($user);
    }
}
