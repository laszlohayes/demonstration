<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Repository\User;

use Parser\Domain\User\Exception\ArticleNotFoundException;
use Parser\Domain\User\Model\Article;
use Parser\Domain\SharedKernel\ValueObject\ArticleId;
use Parser\Domain\User\Repository\ArticleRepositoryInterface;
use Xiglow\InfrastructureBundle\Component\Factory\Interfaces\RepositoryFactoryInterface;
use Xiglow\InfrastructureBundle\Component\Interfaces\RepositoryComponentInterface;

/**
 * Repository for rss.
 */
class ArticleRepository implements ArticleRepositoryInterface
{
    /**
     * @var RepositoryComponentInterface
     */
    private $repositoryComponent;

    /**
     * @param RepositoryFactoryInterface $repositoryFactory
     */
    public function __construct(RepositoryFactoryInterface $repositoryFactory)
    {
        $this->repositoryComponent = $repositoryFactory->create(Article::class);
    }

    /**
     * {@inheritdoc}
     */
    public function ofId(ArticleId $id) : Article
    {
        try {
            return $this->repositoryComponent->find($id);
        } catch (\Exception $exception) {
            throw new ArticleNotFoundException($id->getId());
        }
    }
}
