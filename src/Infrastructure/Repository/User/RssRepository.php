<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Repository\User;

use Parser\Domain\User\Exception\RssNotFoundException;
use Parser\Domain\User\Model\Rss;
use Parser\Domain\SharedKernel\ValueObject\RssId;
use Parser\Domain\User\Repository\RssRepositoryInterface;
use Xiglow\InfrastructureBundle\Component\Factory\Interfaces\RepositoryFactoryInterface;
use Xiglow\InfrastructureBundle\Component\Interfaces\RepositoryComponentInterface;

/**
 * Repository for rss.
 */
class RssRepository implements RssRepositoryInterface
{
    /**
     * @var RepositoryComponentInterface
     */
    private $repositoryComponent;

    /**
     * @param RepositoryFactoryInterface $repositoryFactory
     */
    public function __construct(RepositoryFactoryInterface $repositoryFactory)
    {
        $this->repositoryComponent = $repositoryFactory->create(Rss::class);
    }

    /**
     * {@inheritdoc}
     */
    public function ofId(RssId $id) : Rss
    {
        try {
            return $this->repositoryComponent->find($id);
        } catch (\Exception $exception) {
            throw new RssNotFoundException($id->getId());
        }
    }
}
