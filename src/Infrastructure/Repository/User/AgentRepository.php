<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Repository\User;

use Parser\Domain\SharedKernel\ValueObject\AgentId;
use Parser\Domain\User\Exception\AgentNotFoundException;
use Parser\Domain\User\Model\Agent;
use Parser\Domain\User\Model\User;
use Parser\Domain\User\Repository\AgentRepositoryInterface;
use Xiglow\InfrastructureBundle\Component\Factory\Interfaces\RepositoryFactoryInterface;
use Xiglow\InfrastructureBundle\Component\Interfaces\RepositoryComponentInterface;
use Xiglow\InfrastructureBundle\Exception\ModelNotFoundException;

/**
 * Repository for user.
 */
class AgentRepository implements AgentRepositoryInterface
{
    /**
     * @var RepositoryComponentInterface
     */
    private $repositoryComponent;

    /**
     * @param RepositoryFactoryInterface $repositoryFactory
     */
    public function __construct(RepositoryFactoryInterface $repositoryFactory)
    {
        $this->repositoryComponent = $repositoryFactory->create(User::class);
    }

    /**
     * {@inheritdoc}
     */
    public function ofId(AgentId $id) : Agent
    {
        try {
            $agent = $this->repositoryComponent->find($id);
        } catch (ModelNotFoundException $exception) {
            throw new AgentNotFoundException($id->getId());
        }

        if (!$agent instanceof Agent) {
            throw new AgentNotFoundException($id->getId());
        }

        return $agent;
    }
}
