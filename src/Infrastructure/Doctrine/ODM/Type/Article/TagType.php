<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\Article;

use Parser\Domain\Article\ValueObject\Tag;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for article tag value object.
 */
class TagType extends Type
{
    const NAME = 'article_tag';

    /**
     * {@inheritdoc}
     *
     * @param null|Tag $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? $value->getTag() : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|Tag
     */
    public function convertToPHPValue($value)
    {
        return $value !== null ? new Tag($value) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . Tag::class . '($value);';
    }
}
