<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\Article;

use Parser\Domain\Article\ValueObject\Url;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for article url value object.
 */
class UrlType extends Type
{
    const NAME = 'article_url';

    /**
     * {@inheritdoc}
     *
     * @param null|Url $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? $value->getUrl() : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|Url
     */
    public function convertToPHPValue($value)
    {
        return $value !== null ? new Url($value) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . Url::class . '($value);';
    }
}
