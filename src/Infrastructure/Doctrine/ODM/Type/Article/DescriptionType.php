<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\Article;

use Parser\Domain\Article\ValueObject\Description;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for article description value object.
 */
class DescriptionType extends Type
{
    const NAME = 'article_description';

    /**
     * {@inheritdoc}
     *
     * @param null|Description $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? $value->getDescription() : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|Description
     */
    public function convertToPHPValue($value)
    {
        return $value !== null ? new Description($value) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . Description::class . '($value);';
    }
}
