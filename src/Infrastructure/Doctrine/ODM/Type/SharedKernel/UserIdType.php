<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\SharedKernel;

use Parser\Domain\SharedKernel\ValueObject\UserId;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for user id value object.
 */
class UserIdType extends Type
{
    const NAME = 'user_id';

    /**
     * {@inheritdoc}
     *
     * @param null|UserId $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? (string) $value : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|UserId
     */
    public function convertToPHPValue($value)
    {
        if (!$value instanceof UserId) {
            return new UserId($value);
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . UserId::class . '($value);';
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return self::NAME;
    }
}
