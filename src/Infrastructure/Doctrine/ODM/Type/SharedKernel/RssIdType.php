<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\SharedKernel;

use Parser\Domain\SharedKernel\ValueObject\RssId;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for rss id value object.
 */
class RssIdType extends Type
{
    const NAME = 'rss_id';

    /**
     * {@inheritdoc}
     *
     * @param null|RssId $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? (string) $value : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|RssId
     */
    public function convertToPHPValue($value)
    {
        if (!$value instanceof RssId) {
            return new RssId($value);
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . RssId::class . '($value);';
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return self::NAME;
    }
}
