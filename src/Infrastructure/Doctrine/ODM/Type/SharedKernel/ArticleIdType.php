<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\SharedKernel;

use Parser\Domain\SharedKernel\ValueObject\ArticleId;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for article id value object.
 */
class ArticleIdType extends Type
{
    const NAME = 'article_id';

    /**
     * {@inheritdoc}
     *
     * @param null|ArticleId $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? (string) $value : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|ArticleId
     */
    public function convertToPHPValue($value)
    {
        if (!$value instanceof ArticleId) {
            return new ArticleId($value);
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . ArticleId::class . '($value);';
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return self::NAME;
    }
}
