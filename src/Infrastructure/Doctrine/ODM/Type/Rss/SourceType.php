<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\Rss;

use Parser\Domain\Rss\ValueObject\Source;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for rss source value object.
 */
class SourceType extends Type
{
    const NAME = 'rss_source';

    /**
     * {@inheritdoc}
     *
     * @param null|Source $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? $value->getSource() : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|Source
     */
    public function convertToPHPValue($value)
    {
        return $value !== null ? new Source($value) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . Source::class . '($value);';
    }
}
