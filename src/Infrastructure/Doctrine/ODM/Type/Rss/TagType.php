<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\Rss;

use Parser\Domain\Rss\ValueObject\Tag;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for rss tag value object.
 */
class TagType extends Type
{
    const NAME = 'rss_tag';

    /**
     * {@inheritdoc}
     *
     * @param null|Tag $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? $value->getTag() : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|Tag
     */
    public function convertToPHPValue($value)
    {
        return $value !== null ? new Tag($value) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . Tag::class . '($value);';
    }
}
