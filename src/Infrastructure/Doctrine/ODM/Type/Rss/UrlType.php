<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\Rss;

use Parser\Domain\Rss\ValueObject\Url;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for rss url value object.
 */
class UrlType extends Type
{
    const NAME = 'rss_url';

    /**
     * {@inheritdoc}
     *
     * @param null|Url $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? $value->getUrl() : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|Url
     */
    public function convertToPHPValue($value)
    {
        return $value !== null ? new Url($value) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . Url::class . '($value);';
    }
}
