<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\Rss;

use Parser\Domain\Rss\ValueObject\Title;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for rss title value object.
 */
class TitleType extends Type
{
    const NAME = 'rss_title';

    /**
     * {@inheritdoc}
     *
     * @param null|Title $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? $value->getTitle() : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|Title
     */
    public function convertToPHPValue($value)
    {
        return $value !== null ? new Title($value) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . Title::class . '($value);';
    }
}
