<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\User;

use Parser\Domain\User\ValueObject\UserEmail;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for payment gateway type value object.
 */
class UserEmailType extends Type
{
    const NAME = 'user_email';

    /**
     * {@inheritdoc}
     *
     * @param null|UserEmail $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? $value->getEmail() : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|UserEmail
     */
    public function convertToPHPValue($value)
    {
        return $value !== null ? new UserEmail($value) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . UserEmail::class . '($value);';
    }
}
