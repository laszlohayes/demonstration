<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\User;

use Parser\Domain\User\ValueObject\UserName;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for user name value object.
 */
class UserNameType extends Type
{
    const NAME = 'user_name';

    /**
     * {@inheritdoc}
     *
     * @param null|UserName $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? $value->getName() : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|UserName
     */
    public function convertToPHPValue($value)
    {
        return $value !== null ? new UserName($value) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . UserName::class . '($value);';
    }
}
