<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\Auth;

use Parser\Domain\Auth\ValueObject\ApiKey;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for api key value object.
 */
class ApiKeyType extends Type
{
    const NAME = 'api_key';

    /**
     * {@inheritdoc}
     *
     * @param null|ApiKey $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? $value->getApiKey() : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|ApiKey
     */
    public function convertToPHPValue($value)
    {
        return $value !== null ? new ApiKey($value) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . ApiKey::class . '($value);';
    }
}
