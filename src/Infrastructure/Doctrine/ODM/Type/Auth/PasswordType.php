<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Doctrine\ODM\Type\Auth;

use Parser\Domain\Auth\ValueObject\EncryptedPassword;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Doctrine type for encrypted password value object.
 */
class PasswordType extends Type
{
    const NAME = 'password';

    /**
     * {@inheritdoc}
     *
     * @param null|EncryptedPassword $value
     */
    public function convertToDatabaseValue($value)
    {
        return $value !== null ? $value->getPassword() : null;
    }

    /**
     * {@inheritdoc}
     *
     * @return null|EncryptedPassword
     */
    public function convertToPHPValue($value)
    {
        return $value !== null ? new EncryptedPassword($value) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function closureToMongo()
    {
        return '$return = (string) $value;';
    }

    /**
     * {@inheritdoc}
     */
    public function closureToPHP()
    {
        return '$return = new \\' . EncryptedPassword::class . '($value);';
    }
}
