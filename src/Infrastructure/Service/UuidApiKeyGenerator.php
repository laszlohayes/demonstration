<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Service;

use Parser\Domain\Auth\Service\Interfaces\ApiKeyGeneratorInterface;
use Parser\Domain\Auth\ValueObject\ApiKey;
use Ramsey\Uuid\Uuid;

/**
 * Service for generating API keys using UUID.
 */
class UuidApiKeyGenerator implements ApiKeyGeneratorInterface
{
    /**
     * {@inheritdoc}
     */
    public function generate() : ApiKey
    {
        return new ApiKey(Uuid::uuid4()->toString());
    }
}
