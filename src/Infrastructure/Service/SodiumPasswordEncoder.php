<?php

declare(strict_types=1);

namespace Parser\Infrastructure\Service;

use Parser\Domain\Auth\Service\Interfaces\PasswordEncoderInterface;
use Parser\Domain\Auth\ValueObject\EncryptedPassword;
use Parser\Domain\Auth\ValueObject\PlainPassword;

/**
 * Password encoder service using Sodium.
 */
class SodiumPasswordEncoder implements PasswordEncoderInterface
{
    /**
     * {@inheritdoc}
     */
    public function encode(PlainPassword $password) : EncryptedPassword
    {
        return new EncryptedPassword(sodium_crypto_pwhash_str(
            $password->getPassword(),
            SODIUM_CRYPTO_PWHASH_OPSLIMIT_INTERACTIVE,
            SODIUM_CRYPTO_PWHASH_MEMLIMIT_INTERACTIVE
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function verify(EncryptedPassword $encryptedPassword, PlainPassword $password) : bool
    {
        return sodium_crypto_pwhash_str_verify(
            $encryptedPassword->getPassword(),
            $password->getPassword()
        );
    }
}
