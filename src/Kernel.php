<?php

declare(strict_types=1);

namespace Parser;

use Parser\Infrastructure\Doctrine\ODM\Type\Article\DescriptionType;
use Parser\Infrastructure\Doctrine\ODM\Type\Article\TagType;
use Parser\Infrastructure\Doctrine\ODM\Type\Article\UrlType;
use Parser\Infrastructure\Doctrine\ODM\Type\Auth\ApiKeyType;
use Parser\Infrastructure\Doctrine\ODM\Type\Auth\PasswordType;
use Parser\Infrastructure\Doctrine\ODM\Type\Article\TitleType;
use Parser\Infrastructure\Doctrine\ODM\Type\Rss\SourceType;
use Parser\Infrastructure\Doctrine\ODM\Type\SharedKernel\ArticleIdType;
use Parser\Infrastructure\Doctrine\ODM\Type\SharedKernel\UserIdType;
use Parser\Infrastructure\Doctrine\ODM\Type\SharedKernel\RssIdType;
use Parser\Infrastructure\Doctrine\ODM\Type\Rss\TagType as RssTagType;
use Parser\Infrastructure\Doctrine\ODM\Type\Rss\UrlType as RssUrlType;
use Parser\Infrastructure\Doctrine\ODM\Type\Rss\TitleType as RssTitleType;
use Parser\Infrastructure\Doctrine\ODM\Type\User\UserEmailType;
use Parser\Infrastructure\Doctrine\ODM\Type\User\UserNameType;
use Doctrine\ODM\MongoDB\Types\Type;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\RouteCollectionBuilder;

/**
 * Kernel configuration.
 */
class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    const CONFIG_EXTS = '.{php,xml,yaml,yml}';

    /**
     * List of custom ODM types.
     *
     * @var array
     */
    private $odmTypes = [
        ArticleIdType::class,
        TitleType::class,
        UserIdType::class,
        UserNameType::class,
        UserEmailType::class,
        PasswordType::class,
        ApiKeyType::class,
        RssIdType::class,
        DescriptionType::class,
        TagType::class,
        TitleType::class,
        UrlType::class,
        RssTagType::class,
        RssUrlType::class,
        RssTitleType::class,
        SourceType::class,
    ];

    /**
     * {@inheritdoc}
     */
    public function __construct(string $environment, bool $debug)
    {
        parent::__construct($environment, $debug);

        foreach ($this->odmTypes as $type) {
            Type::registerType($type::NAME, $type);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheDir()
    {
        return $this->getProjectDir() . '/var/cache/' . $this->environment;
    }

    /**
     * {@inheritdoc}
     */
    public function getLogDir()
    {
        return $this->getProjectDir() . '/var/log';
    }

    /**
     * {@inheritdoc}
     */
    public function registerBundles()
    {
        $contents = require $this->getProjectDir() . '/config/bundles.php';
        foreach ($contents as $class => $envs) {
            if (isset($envs['all']) || isset($envs[$this->environment])) {
                yield new $class();
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader)
    {
        $container->addResource(new FileResource($this->getProjectDir() . '/config/bundles.php'));
        // Feel free to remove the "container.autowiring.strict_mode" parameter
        // if you are using symfony/dependency-injection 4.0+ as it's the default behavior.
        $container->setParameter('container.autowiring.strict_mode', true);
        $container->setParameter('container.dumper.inline_class_loader', true);
        $confDir = $this->getProjectDir() . '/config';

        $loader->load($confDir . '/{packages}/*' . self::CONFIG_EXTS, 'glob');
        $loader->load($confDir . '/{packages}/' . $this->environment . '/**/*' . self::CONFIG_EXTS, 'glob');
        $loader->load($confDir . '/{services}' . self::CONFIG_EXTS, 'glob');
        $loader->load($confDir . '/{services}_' . $this->environment . self::CONFIG_EXTS, 'glob');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollectionBuilder $routes)
    {
        $confDir = $this->getProjectDir() . '/config';

        $routes->import($confDir . '/{routes}/*' . self::CONFIG_EXTS, '/', 'glob');
        $routes->import($confDir . '/{routes}/' . $this->environment . '/**/*' . self::CONFIG_EXTS, '/', 'glob');
        $routes->import($confDir . '/{routes}' . self::CONFIG_EXTS, '/', 'glob');
    }
}
