<?php

declare(strict_types=1);

namespace Parser\Application\Service\Source\DTO;

/**
 * DTO for parsed data.
 */
class ParsedData
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $description;

    /**
     * @var array
     */
    private $tags;

    /**
     * @param string $url
     * @param string $title
     * @param string $description
     * @param array  $tags
     */
    public function __construct(string $url, string $title, string $description, array $tags)
    {
        $this->url = $url;
        $this->title = $title;
        $this->description = $description;
        $this->tags = $tags;
    }

    /**
     * @return string
     */
    public function getUrl() : string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getTags() : array
    {
        return $this->tags;
    }
}
