<?php

declare(strict_types=1);

namespace Parser\Application\Service\Source;

use Parser\Application\Service\Source\Interfaces\SourceParserFactoryInterface;
use Parser\Application\Service\Source\Interfaces\SourceParserInterface;
use Parser\Domain\Rss\Exception\InvalidSourceException;
use Parser\Domain\Rss\ValueObject\Source;

/**
 * Factory for creation source parser.
 */
class SourceParserFactory implements SourceParserFactoryInterface
{
    /**
     * @var SourceParserInterface[]
     */
    private $sources = [];

    /**
     * @param iterable|SourceParserInterface[] $sources
     */
    public function __construct(iterable $sources)
    {
        foreach ($sources as $source) {
            $this->addParser($source);
        }
    }

    /**
     * @param Source $source
     *
     * @return SourceParserInterface
     * @throws InvalidSourceException
     */
    public function getParser(Source $source) : SourceParserInterface
    {
        if (!isset($this->sources[$source->getSource()])) {
            throw new InvalidSourceException($source->getSource());
        }

        return $this->sources[$source->getSource()];
    }

    /**
     * @param SourceParserInterface $source
     */
    public function addParser(SourceParserInterface $source) : void
    {
        $this->sources[$source->supports()] = $source;
    }
}
