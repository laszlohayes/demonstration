<?php

declare(strict_types=1);

namespace Parser\Application\Service\Source\Parser;

use Parser\Application\Service\Source\Interfaces\SourceParserInterface;
use Parser\Domain\SharedKernel\Dictionary\SourceType;
use Parser\Application\Service\Source\DTO\ParsedData;

/**
 * Class for parsing Habr rss.
 */
class HabrParser implements SourceParserInterface
{
    /**
     * {@inheritdoc}
     */
    public function parse(\SimpleXMLElement $xml) : array
    {
        $response = [];
        foreach ($xml->channel->item as $item) {
            $response[] = new ParsedData(
                (string) $item->guid,
                (string) $item->title,
                (string) $item->description,
                $this->getTags($item->category)
            );
        }

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return SourceType::HABR;
    }

    /**
     * @param \SimpleXMLElement $category
     *
     * @return array
     */
    private function getTags(\SimpleXMLElement $category) : array
    {
        $categories = [];
        foreach ($category as $item) {
            $categories[] = (string) $item;
        }

        return $categories;
    }
}
