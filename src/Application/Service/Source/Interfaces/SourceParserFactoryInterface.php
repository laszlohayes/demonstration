<?php

declare(strict_types=1);

namespace Parser\Application\Service\Source\Interfaces;

use Parser\Domain\Rss\ValueObject\Source;

/**
 * Interface for origin data response factory.
 */
interface SourceParserFactoryInterface
{
    /**
     * Returns parser.
     *
     * @param Source $source
     *
     * @return SourceParserInterface
     */
    public function getParser(Source $source) : SourceParserInterface;
}
