<?php

declare(strict_types=1);

namespace Parser\Application\Service\Source\Interfaces;

use Parser\Application\Service\Source\DTO\ParsedData;

/**
 * Interface for source parser.
 */
interface SourceParserInterface
{
    /**
     * Returns origin data response.
     *
     * @param \SimpleXMLElement $xml
     *
     * @return ParsedData[]
     */
    public function parse(\SimpleXMLElement $xml) : array;

    /**
     * Returns origin name.
     *
     * @return string
     */
    public function supports() : string;
}
