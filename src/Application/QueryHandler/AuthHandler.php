<?php

declare(strict_types=1);

namespace Parser\Application\QueryHandler;

use Parser\Application\Query\AuthQuery;
use Parser\Application\Response\AuthResponse;
use Parser\Domain\Auth\Repository\AuthRepositoryInterface;
use Parser\Domain\User\Repository\UserRepositoryInterface;
use Xiglow\ApplicationBundle\QueryHandling\Interfaces\QueryHandlerInterface;

/**
 * Handler for user authentication.
 */
class AuthHandler implements QueryHandlerInterface
{
    /**
     * @var AuthRepositoryInterface
     */
    private $authRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @param AuthRepositoryInterface $authRepository
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(AuthRepositoryInterface $authRepository, UserRepositoryInterface $userRepository)
    {
        $this->authRepository = $authRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     *
     * @param AuthQuery $query
     *
     * @return AuthResponse
     */
    public function handle(object $query) : object
    {
        $authUser = $this->authRepository->byApiKey($query->getApiKey());
        $user = $this->userRepository->ofId($authUser->getId());

        return new AuthResponse($user->getId()->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return AuthQuery::class;
    }
}
