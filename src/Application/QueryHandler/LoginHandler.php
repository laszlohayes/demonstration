<?php

declare(strict_types=1);

namespace Parser\Application\QueryHandler;

use Parser\Application\Query\LoginQuery;
use Parser\Application\Response\LoginResponse;
use Parser\Domain\Auth\Exception\BadCredentialsException;
use Parser\Domain\Auth\Exception\UserNotFoundException as AuthUserNotFoundException;
use Parser\Domain\Auth\Repository\AuthRepositoryInterface;
use Parser\Domain\Auth\Service\Interfaces\PasswordEncoderInterface;
use Parser\Domain\Auth\ValueObject\EncryptedPassword;
use Parser\Domain\Auth\ValueObject\PlainPassword;
use Parser\Domain\User\Exception\UserNotFoundException;
use Parser\Domain\User\Repository\UserRepositoryInterface;
use Xiglow\ApplicationBundle\QueryHandling\Interfaces\QueryHandlerInterface;

/**
 * Login query handler.
 */
class LoginHandler implements QueryHandlerInterface
{
    /**
     * @var AuthRepositoryInterface
     */
    private $authRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var PasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @param AuthRepositoryInterface  $authRepository
     * @param UserRepositoryInterface  $userRepository
     * @param PasswordEncoderInterface $passwordEncoder
     */
    public function __construct(
        AuthRepositoryInterface $authRepository,
        UserRepositoryInterface $userRepository,
        PasswordEncoderInterface $passwordEncoder
    )
    {
        $this->authRepository = $authRepository;
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * {@inheritdoc}
     *
     * @param LoginQuery $query
     *
     * @return LoginResponse
     * @throws BadCredentialsException
     */
    public function handle(object $query) : object
    {
        try {
            $user = $this->userRepository->byEmail($query->getEmail());
            $authUser = $this->authRepository->ofId($user->getId());

            $this->verify($authUser->getPassword(), $query->getPassword());
        } catch (AuthUserNotFoundException | UserNotFoundException $exception) {
            throw new BadCredentialsException();
        }

        return new LoginResponse(
            $authUser->getApiKey()->getApiKey(),
            $user->getId()->getId(),
            $user->getType()
        );
    }

    /**
     * Verifies password.
     *
     * @param null|EncryptedPassword $encryptedPassword
     * @param PlainPassword          $plainPassword
     *
     * @throws BadCredentialsException
     */
    private function verify(?EncryptedPassword $encryptedPassword, PlainPassword $plainPassword) : void
    {
        if (!$encryptedPassword || !$this->passwordEncoder->verify($encryptedPassword, $plainPassword)) {
            throw new BadCredentialsException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return LoginQuery::class;
    }
}
