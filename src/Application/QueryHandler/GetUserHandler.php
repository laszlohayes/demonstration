<?php

declare(strict_types=1);

namespace Parser\Application\QueryHandler;

use Parser\Application\Exception\TumblerResponseException;
use Parser\Application\Query\GetUserQuery;
use Parser\Application\Response\GetUserResponse;
use Parser\Application\Service\TumblerClientWrapper\TumblerClientWrapperInterface;
use Parser\Domain\User\Model\Agent;
use Parser\Domain\User\Repository\UserRepositoryInterface;
use Xiglow\ApplicationBundle\QueryHandling\Interfaces\QueryHandlerInterface;

/**
 * Handler for user fetch query.
 */
class GetUserHandler implements QueryHandlerInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @param UserRepositoryInterface       $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     *
     * @param GetUserQuery $query
     *
     * @return GetUserResponse
     */
    public function handle(object $query) : object
    {
        $user = $this->userRepository->ofId($query->getUserId());

        return new GetUserResponse(
            $user->getId()->getId(),
            $user->getName()->getName(),
            $user->getEmail()->getEmail()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return GetUserQuery::class;
    }
}
