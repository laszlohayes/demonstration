<?php

declare(strict_types=1);

namespace Parser\Application\QueryHandler;

use Parser\Application\Query\GetArticlesQuery;
use Parser\Application\Response\ArticleResponse;
use Parser\Application\Response\ArticlesResponse;
use Parser\Application\Response\GetClientResponse;
use Parser\Application\Response\GetClientResponse\GetClientContactPersonResponse;
use Parser\Application\Response\GetClientResponse\GetClientWebsiteResponse;
use Parser\Domain\Article\Model\Article;
use Parser\Domain\Article\Repository\ArticleRepositoryInterface;
use Xiglow\ApplicationBundle\QueryHandling\Interfaces\QueryHandlerInterface;

/**
 * Handler for getting article query.
 */
class GetArticlesHandler implements QueryHandlerInterface
{
    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * @param ArticleRepositoryInterface $articleRepository
     */
    public function __construct(ArticleRepositoryInterface $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * {@inheritdoc}
     *
     * @param GetArticlesQuery $query
     *
     * @return ArticlesResponse
     */
    public function handle(object $query) : object
    {
        $articles = $this->articleRepository->findByUser($query->getUserId());

        $response = new ArticlesResponse();
        array_map(function (Article $article) use ($response) {
            $response->addArticle(new ArticleResponse($article));
        }, $articles);


        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return GetArticlesQuery::class;
    }
}
