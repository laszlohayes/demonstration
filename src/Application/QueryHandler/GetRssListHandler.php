<?php

declare(strict_types=1);

namespace Parser\Application\QueryHandler;

use Parser\Application\Query\GetRssListQuery;
use Parser\Application\Response\RssListResponse;
use Parser\Application\Response\RssResponse;
use Parser\Domain\Rss\Model\Rss;
use Parser\Domain\Rss\Repository\RssRepositoryInterface;
use Xiglow\ApplicationBundle\QueryHandling\Interfaces\QueryHandlerInterface;

/**
 * Handler for getting rss list query.
 */
class GetRssListHandler implements QueryHandlerInterface
{
    /**
     * @var RssRepositoryInterface
     */
    private $rssRepository;

    /**
     * @param RssRepositoryInterface $rssRepository
     */
    public function __construct(RssRepositoryInterface $rssRepository)
    {
        $this->rssRepository = $rssRepository;
    }

    /**
     * {@inheritdoc}
     *
     * @param GetRssListQuery $query
     *
     * @return RssResponse
     */
    public function handle(object $query) : object
    {
        $rss = $this->rssRepository->findByUser($query->getUserId());

        $response = new RssListResponse();
        array_map(function (Rss $rss) use ($response) {
            $response->addRss(new RssResponse($rss));
        }, $rss);


        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return GetRssListQuery::class;
    }
}
