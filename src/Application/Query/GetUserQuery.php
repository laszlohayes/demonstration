<?php

declare(strict_types=1);

namespace Parser\Application\Query;

use Parser\Domain\SharedKernel\ValueObject\UserId;

/**
 * Query for getting user info.
 */
class GetUserQuery
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @param string $userId
     */
    public function __construct(string $userId)
    {
        $this->userId = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function getUserId() : UserId
    {
        return $this->userId;
    }
}
