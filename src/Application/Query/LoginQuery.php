<?php

declare(strict_types=1);

namespace Parser\Application\Query;

use Parser\Domain\Auth\ValueObject\PlainPassword;
use Parser\Domain\SharedKernel\ValueObject\UserEmail;

/**
 * Query for user login.
 */
class LoginQuery
{
    /**
     * @var UserEmail
     */
    private $email;

    /**
     * @var PlainPassword
     */
    private $password;

    /**
     * @param string $email
     * @param string $password
     */
    public function __construct(string $email, string $password)
    {
        $this->email = new UserEmail($email);
        $this->password = new PlainPassword($password);
    }

    /**
     * @return UserEmail
     */
    public function getEmail() : UserEmail
    {
        return $this->email;
    }

    /**
     * @return PlainPassword
     */
    public function getPassword() : PlainPassword
    {
        return $this->password;
    }
}
