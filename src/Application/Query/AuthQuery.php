<?php

declare(strict_types=1);

namespace Parser\Application\Query;

use Parser\Domain\Auth\ValueObject\ApiKey;

/**
 * Query for user auth.
 */
class AuthQuery
{
    /**
     * @var ApiKey
     */
    private $apiKey;

    /**
     * @param string $apiKey
     */
    public function __construct(string $apiKey)
    {
        $this->apiKey = new ApiKey($apiKey);
    }

    /**
     * @return ApiKey
     */
    public function getApiKey() : ApiKey
    {
        return $this->apiKey;
    }
}
