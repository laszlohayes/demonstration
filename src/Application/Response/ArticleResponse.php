<?php

declare(strict_types=1);

namespace Parser\Application\Response;

use Parser\Domain\Article\Model\Article;
use Parser\Domain\Article\ValueObject\Tag;

/**
 * Article fetch query response.
 */
class ArticleResponse
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $description;

    /**
     * @var array
     */
    public $tags;
    
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @param Article $article
     */
    public function __construct(Article $article)
    {
        $this->id = $article->getId()->getId();
        $this->title = $article->getTitle()->getTitle();
        $this->url = $article->getUrl()->getUrl();
        $this->description = $article->getDescription()->getDescription();
        $this->createdAt = $article->getCreatedAt();
        $this->tags = array_map(function (Tag $tag) {
            return $tag->getTag();
        }, $article->getTags());
    }
}
