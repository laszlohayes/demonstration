<?php

declare(strict_types=1);

namespace Parser\Application\Response;

use Parser\Domain\Rss\Model\Rss;
use Parser\Domain\Rss\ValueObject\Tag;

/**
 * Rss fetch query response.
 */
class RssResponse
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $url;

    /**
     * @var array
     */
    public $tags;
    
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @param Rss $article
     */
    public function __construct(Rss $article)
    {
        $this->id = $article->getId()->getId();
        $this->title = $article->getTitle()->getTitle();
        $this->url = $article->getUrl()->getUrl();
        $this->createdAt = $article->getCreatedAt();
        $this->tags = array_map(function (Tag $tag) {
            return $tag->getTag();
        }, $article->getTags());
    }
}
