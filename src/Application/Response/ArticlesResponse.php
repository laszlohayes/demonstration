<?php

declare(strict_types=1);

namespace Parser\Application\Response;

/**
 * Articles fetch query response.
 */
class ArticlesResponse
{
    /**
     * @var []ArticleResponse
     */
    private $articles = [];

    /**
     * @param ArticleResponse $article
     */
    public function addArticle(ArticleResponse $article) : void
    {
        $this->articles[] = $article;
    }

    /**
     * @return array|ArticleResponse[]
     */
    public function getArticles() : array
    {
        return $this->articles;
    }
}
