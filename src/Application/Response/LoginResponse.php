<?php

declare(strict_types=1);

namespace Parser\Application\Response;

/**
 * Login query response.
 */
class LoginResponse
{
    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $type;

    /**
     * @param string $apiKey
     * @param string $userId
     * @param string $type
     */
    public function __construct(string $apiKey, string $userId, string $type)
    {
        $this->apiKey = $apiKey;
        $this->userId = $userId;
        $this->type = $type;
    }
}
