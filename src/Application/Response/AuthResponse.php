<?php

declare(strict_types=1);

namespace Parser\Application\Response;

/**
 * Authentication query response.
 */
class AuthResponse
{
    /**
     * @var string
     */
    public $userId;

    /**
     * @param string $userId
     */
    public function __construct(string $userId)
    {
        $this->userId = $userId;
    }
}
