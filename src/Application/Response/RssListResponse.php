<?php

declare(strict_types=1);

namespace Parser\Application\Response;

/**
 * Rss list fetch query response.
 */
class RssListResponse
{
    /**
     * @var []RssResponse
     */
    private $rss = [];

    /**
     * @param RssResponse $rss
     */
    public function addRss(RssResponse $rss) : void
    {
        $this->rss[] = $rss;
    }

    /**
     * @return array|RssResponse[]
     */
    public function getRssList() : array
    {
        return $this->rss;
    }
}
