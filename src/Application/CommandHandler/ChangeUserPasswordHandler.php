<?php

declare(strict_types=1);

namespace Parser\Application\CommandHandler;

use Parser\Application\Command\ChangeUserPasswordCommand;
use Parser\Domain\Auth\Repository\AuthRepositoryInterface;
use Parser\Domain\Auth\Service\Interfaces\PasswordEncoderInterface;
use Xiglow\ApplicationBundle\CommandHandling\Interfaces\CommandHandlerInterface;

/**
 * Handler for changing user password.
 */
class ChangeUserPasswordHandler implements CommandHandlerInterface
{
    /**
     * @var AuthRepositoryInterface
     */
    private $authRepository;

    /**
     * @var PasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @param AuthRepositoryInterface  $authRepository
     * @param PasswordEncoderInterface $passwordEncoder
     */
    public function __construct(AuthRepositoryInterface $authRepository, PasswordEncoderInterface $passwordEncoder)
    {
        $this->authRepository = $authRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * {@inheritdoc}
     *
     * @param ChangeUserPasswordCommand $command
     */
    public function handle(object $command) : void
    {
        $password = $this->passwordEncoder->encode($command->getPlainPassword());

        $user = $this->authRepository->ofId($command->getUserId());
        $user->setPassword($password);

        $this->authRepository->save($user);
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return ChangeUserPasswordCommand::class;
    }
}
