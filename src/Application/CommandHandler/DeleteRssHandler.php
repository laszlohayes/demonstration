<?php

declare(strict_types=1);

namespace Parser\Application\CommandHandler;

use Parser\Application\Command\DeleteRssCommand;
use Parser\Domain\Rss\Exception\RssAlreadyExistsException;
use Parser\Domain\Rss\Repository\RssRepositoryInterface;
use Xiglow\ApplicationBundle\CommandHandling\Interfaces\CommandHandlerInterface;

/**
 * Handler for creating rss command.
 */
class DeleteRssHandler implements CommandHandlerInterface
{
    /**
     * @var RssRepositoryInterface
     */
    private $rssRepository;

    /**
     * @param RssRepositoryInterface $rssRepository
     */
    public function __construct(RssRepositoryInterface $rssRepository)
    {
        $this->rssRepository = $rssRepository;
    }

    /**
     * {@inheritdoc}
     *
     * @param DeleteRssCommand $command
     *
     * @throws RssAlreadyExistsException
     */
    public function handle(object $command) : void
    {
        $rss = $this->rssRepository->ofId($command->getRssId());

        $this->rssRepository->delete($rss);
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return DeleteRssCommand::class;
    }
}
