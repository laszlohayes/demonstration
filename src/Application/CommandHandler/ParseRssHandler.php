<?php

declare(strict_types=1);

namespace Parser\Application\CommandHandler;

use Parser\Application\Command\CreateArticleCommand;
use Parser\Application\Command\ParseCommand;
use Parser\Application\Service\Source\Interfaces\SourceParserFactoryInterface;
use Parser\Domain\Rss\Repository\RssRepositoryInterface;
use Ramsey\Uuid\Uuid;
use Xiglow\ApplicationBundle\CommandHandling\Interfaces\CommandHandlerInterface;

/**
 * Handler for parsing rss command.
 */
class ParseRssHandler implements CommandHandlerInterface
{
    /**
     * @var RssRepositoryInterface
     */
    private $rssRepository;

    /**
     * @var CreateArticleHandler
     */
    private $createArticleHandler;

    /**
     * @var SourceParserFactoryInterface
     */
    private $sourceParserFactory;

    /**
     * @param RssRepositoryInterface       $rssRepository
     * @param CreateArticleHandler         $createArticleHandler
     * @param SourceParserFactoryInterface $sourceParserFactory
     */
    public function __construct(
        RssRepositoryInterface $rssRepository,
        CreateArticleHandler $createArticleHandler,
        SourceParserFactoryInterface $sourceParserFactory
    )
    {
        $this->rssRepository = $rssRepository;
        $this->createArticleHandler = $createArticleHandler;
        $this->sourceParserFactory = $sourceParserFactory;
    }

    /**
     * {@inheritdoc}
     *
     * @param ParseCommand $command
     */
    public function handle(object $command) : void
    {
        $rss = $this->rssRepository->findAll();

        foreach ($rss as $data) {
            $parser = $this->sourceParserFactory->getParser($data->getSource());
            $xml = simplexml_load_file($data->getUrl()->getUrl());
            $result = $parser->parse($xml);

            foreach ($result as $item) {
                if (!array_intersect($data->getTags(), $item->getTags())) {
                    continue;
                }
                $this->createArticleHandler->handle(new CreateArticleCommand(
                    $data->getUserId()->getId(),
                    Uuid::uuid4()->toString(),
                    $item->getUrl(),
                    $item->getTitle(),
                    $item->getDescription(),
                    $item->getTags()
                ));
                break;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return ParseCommand::class;
    }
}
