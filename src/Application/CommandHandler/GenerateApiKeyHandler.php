<?php

declare(strict_types=1);

namespace Parser\Application\CommandHandler;

use Parser\Application\Command\GenerateApiKeyCommand;
use Parser\Domain\Auth\Repository\AuthRepositoryInterface;
use Parser\Domain\Auth\Service\Interfaces\ApiKeyGeneratorInterface;
use Xiglow\ApplicationBundle\CommandHandling\Interfaces\CommandHandlerInterface;

/**
 * Handler for generating api key.
 */
class GenerateApiKeyHandler implements CommandHandlerInterface
{
    /**
     * @var AuthRepositoryInterface
     */
    private $authRepository;

    /**
     * @var ApiKeyGeneratorInterface
     */
    private $apiKeyGenerator;

    /**
     * @param AuthRepositoryInterface  $authRepository
     * @param ApiKeyGeneratorInterface $apiKeyGenerator
     */
    public function __construct(AuthRepositoryInterface $authRepository, ApiKeyGeneratorInterface $apiKeyGenerator)
    {
        $this->authRepository = $authRepository;
        $this->apiKeyGenerator = $apiKeyGenerator;
    }

    /**
     * {@inheritdoc}
     *
     * @param GenerateApiKeyCommand $command
     */
    public function handle(object $command) : void
    {
        $user = $this->authRepository->ofId($command->getUserId());
        $user->setApiKey($this->apiKeyGenerator->generate());

        $this->authRepository->save($user);
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return GenerateApiKeyCommand::class;
    }
}
