<?php

declare(strict_types=1);

namespace Parser\Application\CommandHandler;

use Parser\Application\Command\UpdateRssCommand;
use Parser\Domain\Rss\Repository\RssRepositoryInterface;
use Xiglow\ApplicationBundle\CommandHandling\Interfaces\CommandHandlerInterface;

/**
 * Handler for update rss command.
 */
class UpdateRssHandler implements CommandHandlerInterface
{
    /**
     * @var RssRepositoryInterface
     */
    private $rssRepository;

    /**
     * @param RssRepositoryInterface $rssRepository
     */
    public function __construct(RssRepositoryInterface $rssRepository)
    {
        $this->rssRepository = $rssRepository;
    }

    /**
     * {@inheritdoc}
     *
     * @param UpdateRssCommand $command
     */
    public function handle(object $command) : void
    {
        $rss = $this->rssRepository->ofId($command->getRssId());

        $rss->setUrl($command->getUrl());
        $rss->setTitle($command->getTitle());
        $rss->setTags($command->getTags());

        $this->rssRepository->save($rss);
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return UpdateRssCommand::class;
    }
}
