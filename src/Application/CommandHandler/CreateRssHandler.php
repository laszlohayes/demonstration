<?php

declare(strict_types=1);

namespace Parser\Application\CommandHandler;

use Parser\Application\Command\CreateRssCommand;
use Parser\Domain\Rss\Exception\RssAlreadyExistsException;
use Parser\Domain\Rss\Exception\RssNotFoundException;
use Parser\Domain\Rss\Model\Rss;
use Parser\Domain\Rss\Repository\RssRepositoryInterface;
use Xiglow\ApplicationBundle\CommandHandling\Interfaces\CommandHandlerInterface;

/**
 * Handler for creating rss command.
 */
class CreateRssHandler implements CommandHandlerInterface
{
    /**
     * @var RssRepositoryInterface
     */
    private $rssRepository;

    /**
     * @param RssRepositoryInterface $rssRepository
     */
    public function __construct(RssRepositoryInterface $rssRepository)
    {
        $this->rssRepository = $rssRepository;
    }

    /**
     * {@inheritdoc}
     *
     * @param CreateRssCommand $command
     *
     * @throws RssAlreadyExistsException
     */
    public function handle(object $command) : void
    {
        try {
            $this->rssRepository->findByUrl($command->getUrl());
            throw new RssAlreadyExistsException($command->getName());
        } catch (RssNotFoundException $exception) {
            $this->rssRepository->save(
                new Rss(
                    $command->getRssId(),
                    $command->getUserId(),
                    $command->getSource(),
                    $command->getTitle(),
                    $command->getUrl(),
                    $command->getTags()
                )
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return CreateRssCommand::class;
    }
}
