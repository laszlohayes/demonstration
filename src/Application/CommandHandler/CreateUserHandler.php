<?php

declare(strict_types=1);

namespace Parser\Application\CommandHandler;

use Parser\Application\Command\GenerateApiKeyCommand;
use Parser\Application\Command\CreateUserCommand;
use Parser\Application\Command\ChangeUserPasswordCommand;
use Parser\Domain\User\Exception\UserAlreadyExistsException;
use Parser\Domain\User\Exception\UserCreationException;
use Parser\Domain\User\Exception\UserNotFoundException;
use Parser\Domain\User\Model\User;
use Parser\Domain\User\Repository\UserRepositoryInterface;
use Xiglow\ApplicationBundle\CommandHandling\Interfaces\CommandHandlerInterface;

/**
 * Handler for creating user command.
 */
class CreateUserHandler implements CommandHandlerInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var ChangeUserPasswordHandler
     */
    private $changeUserPasswordHandler;

    /**
     * @var GenerateApiKeyHandler
     */
    private $generateApiKeyHandler;

    /**
     * @param UserRepositoryInterface   $userRepository
     * @param ChangeUserPasswordHandler $changeUserPasswordHandler
     * @param GenerateApiKeyHandler     $generateApiKeyHandler
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        ChangeUserPasswordHandler $changeUserPasswordHandler,
        GenerateApiKeyHandler $generateApiKeyHandler
    )
    {
        $this->userRepository = $userRepository;
        $this->changeUserPasswordHandler = $changeUserPasswordHandler;
        $this->generateApiKeyHandler = $generateApiKeyHandler;
    }

    /**
     * {@inheritdoc}
     *
     * @param CreateUserCommand $command
     *
     * @throws UserAlreadyExistsException
     * @throws UserCreationException
     */
    public function handle(object $command) : void
    {
        try {
            $this->userRepository->byEmail($command->getEmail());

            // Fail if user exists.
            throw new UserAlreadyExistsException($command->getEmail()->getEmail());
        } catch (UserNotFoundException $exception) {
            // Continue handling command.
        }

        $agent = new User($command->getId(), $command->getName(), $command->getEmail());
        $this->userRepository->save($agent);

        try {
            $this->changeUserPasswordHandler->handle(
                new ChangeUserPasswordCommand($agent->getId()->getId(), $command->getPassword()->getPassword())
            );

            $this->generateApiKeyHandler->handle(
                new GenerateApiKeyCommand($agent->getId()->getId())
            );
        } catch (\Throwable $exception) {
            $this->userRepository->forceDelete($agent);

            throw new UserCreationException($exception->getMessage());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return CreateUserCommand::class;
    }
}
