<?php

declare(strict_types=1);

namespace Parser\Application\CommandHandler;

use Parser\Application\Command\CreateArticleCommand;
use Parser\Domain\Article\Exception\ArticleAlreadyExistsException;
use Parser\Domain\Article\Exception\ArticleNotFoundException;
use Parser\Domain\Article\Model\Article;
use Parser\Domain\Article\Repository\ArticleRepositoryInterface;
use Xiglow\ApplicationBundle\CommandHandling\Interfaces\CommandHandlerInterface;

/**
 * Handler for creating article command.
 */
class CreateArticleHandler implements CommandHandlerInterface
{
    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * @param ArticleRepositoryInterface $articleRepository
     */
    public function __construct(ArticleRepositoryInterface $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * {@inheritdoc}
     *
     * @param CreateArticleCommand $command
     *
     * @throws ArticleAlreadyExistsException
     */
    public function handle(object $command) : void
    {
        try {
            $this->articleRepository->findByUrl($command->getUrl());
            throw new ArticleAlreadyExistsException($command->getTitle()->getTitle());
        } catch (ArticleNotFoundException $exception) {
            $this->articleRepository->save(
                new Article(
                    $command->getArticleId(),
                    $command->getTitle(),
                    $command->getUrl(),
                    $command->getDescription(),
                    $command->getTags(),
                    $command->getUserId()
                )
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function supports() : string
    {
        return CreateArticleCommand::class;
    }
}
