<?php

declare(strict_types=1);

namespace Parser\Application\Command;

use Parser\Domain\SharedKernel\ValueObject\UserId;

/**
 * Command for generating api key.
 */
class GenerateApiKeyCommand
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @param string $userId
     */
    public function __construct(string $userId)
    {
        $this->userId = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function getUserId() : UserId
    {
        return $this->userId;
    }
}
