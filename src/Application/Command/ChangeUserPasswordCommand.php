<?php

declare(strict_types=1);

namespace Parser\Application\Command;

use Parser\Domain\Auth\ValueObject\PlainPassword;
use Parser\Domain\SharedKernel\ValueObject\UserId;

/**
 * Command for changing user password.
 */
class ChangeUserPasswordCommand
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var PlainPassword
     */
    private $password;

    /**
     * @param string $userId
     * @param string $password
     */
    public function __construct(string $userId, string $password)
    {
        $this->userId = new UserId($userId);
        $this->password = new PlainPassword($password);
    }

    /**
     * @return PlainPassword
     */
    public function getPlainPassword() : PlainPassword
    {
        return $this->password;
    }

    /**
     * @return UserId
     */
    public function getUserId() : UserId
    {
        return $this->userId;
    }
}
