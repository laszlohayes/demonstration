<?php

declare(strict_types=1);

namespace Parser\Application\Command;

use Parser\Domain\Rss\ValueObject\Tag;
use Parser\Domain\Rss\ValueObject\Title;
use Parser\Domain\Rss\ValueObject\Url;
use Parser\Domain\SharedKernel\ValueObject\RssId;
use Parser\Domain\SharedKernel\ValueObject\UserId;

/**
 * Command for updating rss.
 */
class UpdateRssCommand
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var RssId
     */
    private $rssId;

    /**
     * @var Url
     */
    private $url;

    /**
     * @var Title
     */
    private $title;

    /**
     * @var Tag[]
     */
    private $tags;

    /**
     * @param string $userId
     * @param string $rssId
     * @param string $url
     * @param string $title
     * @param array  $tags
     */
    public function __construct(
        string $userId,
        string $rssId,
        string $url,
        string $title,
        array $tags
    )
    {
        $this->userId = new UserId($userId);
        $this->rssId = new RssId($rssId);
        $this->url = new Url($url);
        $this->title = new Title($title);
        array_map(function ($tag) {
            $this->tags[] = new Tag($tag);
        }, $tags);
    }

    /**
     * @return UserId
     */
    public function getUserId() : UserId
    {
        return $this->userId;
    }

    /**
     * @return RssId
     */
    public function getRssId() : RssId
    {
        return $this->rssId;
    }

    /**
     * @return Url
     */
    public function getUrl() : Url
    {
        return $this->url;
    }

    /**
     * @return Title
     */
    public function getTitle() : Title
    {
        return $this->title;
    }

    /**
     * @return Tag[]
     */
    public function getTags() : array
    {
        return $this->tags;
    }
}
