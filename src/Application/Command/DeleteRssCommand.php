<?php

declare(strict_types=1);

namespace Parser\Application\Command;

use Parser\Domain\Rss\ValueObject\Tag;
use Parser\Domain\Rss\ValueObject\Title;
use Parser\Domain\Rss\ValueObject\Url;
use Parser\Domain\SharedKernel\ValueObject\RssId;
use Parser\Domain\SharedKernel\ValueObject\UserId;

/**
 * Command for deleting rss.
 */
class DeleteRssCommand
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var RssId
     */
    private $rssId;

    /**
     * @param string $userId
     * @param string $rssId
     */
    public function __construct(string $userId, string $rssId)
    {
        $this->userId = new UserId($userId);
        $this->rssId = new RssId($rssId);
    }

    /**
     * @return UserId
     */
    public function getUserId() : UserId
    {
        return $this->userId;
    }

    /**
     * @return RssId
     */
    public function getRssId() : RssId
    {
        return $this->rssId;
    }
}
