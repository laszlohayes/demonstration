<?php

declare(strict_types=1);

namespace Parser\Application\Command;

use Parser\Domain\Auth\ValueObject\PlainPassword;
use Parser\Domain\User\ValueObject\UserEmail;
use Parser\Domain\User\ValueObject\UserName;
use Parser\Domain\SharedKernel\ValueObject\UserId;

/**
 * Command for creating user.
 */
class CreateUserCommand
{
    /**
     * @var UserId
     */
    private $id;

    /**
     * @var UserName
     */
    private $name;

    /**
     * @var UserEmail
     */
    private $email;

    /**
     * @var PlainPassword
     */
    private $password;

    /**
     * @param string $id
     * @param string $name
     * @param string $email
     * @param string $password
     */
    public function __construct(string $id, string $name, string $email, string $password)
    {
        $this->id = new UserId($id);
        $this->name = new UserName($name);
        $this->email = new UserEmail($email);
        $this->password = new PlainPassword($password);
    }

    /**
     * @return UserId
     */
    public function getId() : UserId
    {
        return $this->id;
    }

    /**
     * @return UserName
     */
    public function getName() : UserName
    {
        return $this->name;
    }

    /**
     * @return UserEmail
     */
    public function getEmail() : UserEmail
    {
        return $this->email;
    }

    /**
     * @return PlainPassword
     */
    public function getPassword() : PlainPassword
    {
        return $this->password;
    }
}
