<?php

declare(strict_types=1);

namespace Parser\Application\Command;

use Parser\Domain\Article\ValueObject\Description;
use Parser\Domain\Article\ValueObject\Tag;
use Parser\Domain\Article\ValueObject\Title;
use Parser\Domain\Article\ValueObject\Url;
use Parser\Domain\SharedKernel\ValueObject\ArticleId;
use Parser\Domain\SharedKernel\ValueObject\UserId;

/**
 * Command for creating article.
 */
class CreateArticleCommand
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var ArticleId
     */
    private $articleId;

    /**
     * @var Url
     */
    private $url;

    /**
     * @var Title
     */
    private $title;

    /**
     * @var Description
     */
    private $description;

    /**
     * @var Tag[]
     */
    private $tags;

    /**
     * @param string $userId
     * @param string $articleId
     * @param string $url
     * @param string $title
     * @param string $description
     * @param array  $tags
     */
    public function __construct(
        string $userId,
        string $articleId,
        string $url,
        string $title,
        string $description,
        array $tags
    )
    {
        $this->userId = new UserId($userId);
        $this->articleId = new ArticleId($articleId);
        $this->url = new Url($url);
        $this->title = new Title($title);
        $this->description = new Description($description);
        array_map(function ($tag) {
            $this->tags[] = new Tag($tag);
        }, $tags);
    }

    /**
     * @return UserId
     */
    public function getUserId() : UserId
    {
        return $this->userId;
    }

    /**
     * @return ArticleId
     */
    public function getArticleId() : ArticleId
    {
        return $this->articleId;
    }

    /**
     * @return Url
     */
    public function getUrl() : Url
    {
        return $this->url;
    }

    /**
     * @return Title
     */
    public function getTitle() : Title
    {
        return $this->title;
    }

    /**
     * @return Description
     */
    public function getDescription() : Description
    {
        return $this->description;
    }

    /**
     * @return null|Tag[]
     */
    public function getTags() : ?array
    {
        return $this->tags;
    }
}
